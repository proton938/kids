import { Component, Input, OnInit } from '@angular/core';
import * as _ from 'lodash';

import { Report } from '../models/report.model';
import { ErrorComponent } from 'src/app/modals/error.component';
import { isStringValid } from '../utils/validation.utils';
import { BsModalService } from 'ngx-bootstrap';

@Component({
  selector: 'app-step-rates',
  templateUrl: './rates.component.html',
  styleUrls: ['./rates.component.scss'],
})
export class RatesComponent implements OnInit {
  @Input() report: Report = new Report();

  loading = false;
  showErrors = false;

  currentStep = 'h1';

  showNss = false;

  constructor(private modalService: BsModalService) {
  }

  ngOnInit() {
    const oldRulesDate = new Date('07.01.2018').getTime();

    /*
    this.showNss = !this.report.Appendix2s.some((appendix) => {
      return appendix.buildingPermits.some((buildingPermit) => {
        return new Date(buildingPermit.permitDate).getTime() < oldRulesDate;
      });
    });
    */

  }

  isH1Valid() {
    if (this.report.buildingOutsideOfMoscow === 'true') {
      return isStringValid(this.report.b16000);
    }

    return true;
  }

  showH1Error() {
    this.modalService.show(ErrorComponent, {
      initialState: {
        title: `
          <span class="mandatory">Некорректно заполнены обязательные поля.</span><br/>
          Ознакомьтесь с правилами заполнения данных формы:
          `,
        message: `
            <h3>Раздел "3. Расчет нормативов"</h3>
            <span class="mandatory">*</span> - выбор варианта обязателен.
          `,
      }
    });
  }

  isH2Valid() {
    let hasMissedChechbox = false;
    if (!this.report.withoutUnfinishedBuildingActivities) {
      const hasCheckbox = this.report.balanceNodes.assets.some((row) => (row.unfinishedBuildingActivities));
      hasMissedChechbox = hasMissedChechbox || !hasCheckbox;
    }
    if (!this.report.withoutContractDebts) {
      const hasCheckbox = this.report.balanceNodes.assets.some((row) => (row.contractDebts));
      hasMissedChechbox = hasMissedChechbox || !hasCheckbox;
    }
    if (!this.report.withoutContractLiabilities) {
      const hasCheckbox = this.report.balanceNodes.liabilities.some((row) => (row.contractLiabilities));
      hasMissedChechbox = hasMissedChechbox || !hasCheckbox;
    }
    if (!this.report.withoutDebtOfFounders) {
      const hasCheckbox = this.report.balanceNodes.assets.some((row) => (row.debtOfFounders));
      hasMissedChechbox = hasMissedChechbox || !hasCheckbox;
    }
    if (!this.report.withoutRevenueOfFuturePeriods) {
      const hasCheckbox = this.report.balanceNodes.liabilities.some((row) => (row.revenueOfFuturePeriods));
      hasMissedChechbox = hasMissedChechbox || !hasCheckbox;
    }

    if (hasMissedChechbox) {
      return false;
    }

    return true;
  }

  showH2Error() {
    this.modalService.show(ErrorComponent, {
      initialState: {
        title: `
        <span class="mandatory">Некорректно заполнены обязательные поля.</span><br/>
        Ознакомьтесь с правилами заполнения данных формы:
        `,
        message: `
          <h3>Раздел "3. Расчет нормативов"</h3>
          <p>
          Ознакомьтесь с правилами заполнения формы для расчета норматива Н2: необходимо выбрать строки, участвующие в расчете норматива, установив флаг в соответствующие ячейки.
          </p>
          <p>
          В каждом столбце должен быть установлен хотя бы один флаг.
          </p>
          <p>
          Если строки не участвуют в расчете или отсутствуют на форме, то необходимо в конце соответствующего раздела установить флаг в строке «Строки, участвующие в расчете, отсутствуют».
          </p>
        `,
      }
    });
  }

  isNssValid() {
    const oldRulesDate = new Date('07.01.2018').getTime();
    const showNss = !this.report.Appendix2s.some((appendix) => {
      return appendix.buildingPermits.some((buildingPermit) => {
        return new Date(buildingPermit.permitDate).getTime() < oldRulesDate;
      });
    });

    if (!showNss) {
      return true;
    }

    let hasMissedChechbox = false;

    if (!this.report.withoutUnfinishedBuildingActivities673) {
      const hasCheckbox = this.report.balanceNodes.assets.some((row) => (row.unfinishedBuildingActivities673));
      hasMissedChechbox = hasMissedChechbox || !hasCheckbox;
    }
    if (!this.report.withoutTaxesAndFees673) {
      const hasCheckbox = this.report.balanceNodes.assets.some((row) => (row.taxesAndFees673));
      hasMissedChechbox = hasMissedChechbox || !hasCheckbox;
    }
    if (!this.report.withoutFinishedBuildingActivities673) {
      const hasCheckbox = this.report.balanceNodes.assets.some((row) => (row.finishedBuildingActivities673));
      hasMissedChechbox = hasMissedChechbox || !hasCheckbox;
    }
    if (!this.report.withoutPrepayment673) {
      const hasCheckbox = this.report.balanceNodes.assets.some((row) => (row.prepayment673));
      hasMissedChechbox = hasMissedChechbox || !hasCheckbox;
    }
    if (!this.report.withoutOtherCosts673) {
      const hasCheckbox = this.report.balanceNodes.assets.some((row) => (row.otherCosts673));
      hasMissedChechbox = hasMissedChechbox || !hasCheckbox;
    }
    if (!this.report.withoutContractDebts673) {
      const hasCheckbox = this.report.balanceNodes.assets.some((row) => (row.contractDebts673));
      hasMissedChechbox = hasMissedChechbox || !hasCheckbox;
    }
    if (!this.report.withoutValueAddedTax673) {
      const hasCheckbox = this.report.balanceNodes.assets.some((row) => (row.valueAddedTax673));
      hasMissedChechbox = hasMissedChechbox || !hasCheckbox;
    }
    if (!this.report.withoutCashInBankAccounts673) {
      const hasCheckbox = this.report.balanceNodes.assets.some((row) => (row.cashInBankAccounts673));
      hasMissedChechbox = hasMissedChechbox || !hasCheckbox;
    }
    if (!this.report.withoutRevenueOfFuturePeriods673) {
      const hasCheckbox = this.report.balanceNodes.liabilities.some((row) => (row.revenueOfFuturePeriods673));
      hasMissedChechbox = hasMissedChechbox || !hasCheckbox;
    }

    if (hasMissedChechbox) {
      return false;
    }

    return true;
  }

  showNssError() {
    this.modalService.show(ErrorComponent, {
      initialState: {
        title: `
        <span class="mandatory">Некорректно заполнены обязательные поля.</span><br/>
        Ознакомьтесь с правилами заполнения данных формы:
        `,
        message: `
          <h3>Раздел "3. Расчет нормативов"</h3>
          <p>
          Ознакомьтесь с правилами заполнения формы для расчета норматива Н2: необходимо выбрать строки, участвующие в расчете норматива, установив флаг в соответствующие ячейки.
          </p>
          <p>
          Для выбора доступны только те строки бухгалтерского баланса, которые соответствуют Постановлению Правительства РФ от 11.06.2018 №673.
          </p>
          <p>
          В каждом столбце должен быть установлен хотя бы один флаг.
          </p>
          <p>
          Если строки не участвуют в расчете или отсутствуют на форме, то необходимо в конце соответствующего раздела установить флаг в строке «Строки, участвующие в расчете, отсутствуют».
          </p>
        `,
      }
    });
  }

  changeStep(newStep) {
    if (newStep === this.currentStep) {
      return;
    }
    /*
    if (this.currentStep === 'h1') {
      if (!this.isH1Valid()) {
        this.showH1Error();
        this.showErrors = true;
        return;
      }
    }

    if (this.currentStep === 'h2' && newStep !== 'h1')  {
      if (!this.isH2Valid()) {
        this.showH2Error();
        this.showErrors = true;
        return;
      }
    }
    */
    this.showErrors = false;
    this.currentStep = newStep;
  }

  public isValid() {
    /*
    if (!this.isH1Valid()) {
      this.showH1Error();
      this.showErrors = true;

      return false;
    }

    if (!this.isH2Valid()) {
      this.showH2Error();
      this.showErrors = true;

      return false;
    }

    if (!this.isNssValid()) {
      this.showNssError();
      this.showErrors = true;

      return false;
    }
    */
    return true;
  }
}
