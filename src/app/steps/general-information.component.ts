import { Component, Input, ViewChild, OnInit } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { HttpClient } from '@angular/common/http';

import { Report, Appendix2 } from '../models/report.model';
import { InformationAboutThePropertyComponent } from '../modals/information-about-the-property.component';
import { forkJoin } from 'rxjs';
import { GeneralInformationErrorComponent } from '../modals/general-information-error.component';
import _ = require('lodash');
import { isStringValid, isINNValid } from 'src/app/utils/validation.utils';
import { ReportService } from "../models/report.service";

@Component({
  selector: 'app-step-general-information',
  templateUrl: './general-information.component.html',
})
export class GeneralInformationComponent implements OnInit {
  @Input() report: Report;
  @Input() importXML: () => void;

  public years: string[] = ['2019', '2020', '2021', '2022', '2023', '2024', '2025'];
  public ikvartals: any[] = [
    {
      name: '1 квартал',
      value: 1,
    },
    {
      name: '2 квартал',
      value: 2,
    },
    {
      name: '3 квартал',
      value: 3,
    },
    {
      name: '4 квартал',
      value: 4,
    },
  ];

  subjects: any[] = [];
  settlementKinds: any[] = [];
  roadNetworkElements: any[] = [];
  buildingTypes: any[] = [];
  roomTypes: any[] = [];
  rightKinds: any[] = [];
  innRnsMap: any = {};
  showErrors = false;
  showInnError = false;
  showInn2Error = false;

  constructor(private modalService: BsModalService, private httpService: HttpClient, private reportService: ReportService) {
  }

  ngOnInit() {
    this.initDicts();
  }


  onChangeSeaIsOrganization(newValue) {
    setTimeout(() => {
      if (newValue === 'true') {
        this.report.seaPersonFirstName = '';
        this.report.seaPersonLastName = '';
        this.report.seaPersonMiddleName = '';
      } else {
        this.report.seaOrganizationName = '';
        this.report.seaOrganizationINN = '';
      }
    });
  }

  addAppendix() {
    const initialState = {
      appendix2: new Appendix2(),
      rightKinds: this.rightKinds,
      innRnsMap: this.innRnsMap,
      inn: this.report.INN,
      ikvartal: this.report.ikvartal,
      year: this.report.year,
    };
    const modal = this.modalService.show(InformationAboutThePropertyComponent, { class: 'modal-max', initialState });

    modal.content.event.subscribe((appendix2) => {
      // @ts-ignore
      this.report.Appendix2s.push(appendix2);
    });

  }

  removeAppendix(index) {
    // @ts-ignore
    this.report.Appendix2s.splice(index, 1);
  }



  editAppendix(index) {
    let correctJson = this.report.Appendix2s[index];
    let onArray = correctJson.cadastralNumber instanceof Array;
    if (!onArray) {
      let buffer: any = correctJson.cadastralNumber;
      correctJson.cadastralNumber = [];
      correctJson.cadastralNumber.push(buffer);
    }
    if (correctJson.creditOrganizations === undefined) {
      correctJson['creditOrganizations'] = [];
    }

    const initialState = {
      appendix2: JSON.parse(JSON.stringify(correctJson)),
      rightKinds: this.rightKinds,
      innRnsMap: this.innRnsMap,
      inn: this.report.INN,
      ikvartal: this.report.ikvartal,
      year: this.report.year,
    };
    const modal = this.modalService.show(InformationAboutThePropertyComponent, { class: 'modal-max', initialState });

    modal.content.event.subscribe((appendix2) => {
      this.report.Appendix2s[index] = appendix2;
    });
  }

  initDicts() {
    forkJoin([
      this.httpService.get('assets/dicts/building.json'),
      this.httpService.get('assets/dicts/right.json'),
      this.httpService.get('assets/dicts/road.json'),
      this.httpService.get('assets/dicts/room.json'),
      this.httpService.get('assets/dicts/settlement.json'),
      this.httpService.get('assets/dicts/subjectRF.json'),
      this.httpService.get('assets/dicts/inn_rns_numbers.json'),
    ]).subscribe((dicts: any[]) => {
      this.buildingTypes = dicts[0];
      this.rightKinds = dicts[1];
      this.roadNetworkElements = dicts[2];
      this.roomTypes = dicts[3];
      this.settlementKinds = dicts[4];
      this.subjects = dicts[5];
      this.innRnsMap = dicts[6];
    });
  }

  load() {
    this.importXML();
  }

  public isValid() {
    if (!this.report.ikvartal) {
      return false;
    }

    if (!this.report.year) {
      return false;
    }

    if (this.report.seaIsOrganization === 'true') {
      if (!isStringValid(this.report.organizationName)) {
        return false;
      }

      if (!isINNValid(this.report.seaOrganizationINN)) {
        this.showInnError = true;
        return false;
      }
    } else {
      if (!isStringValid(this.report.seaPersonFirstName)) {
        return false;
      }

      if (!isStringValid(this.report.seaPersonLastName)) {
        return false;
      }
    }

    if (!isStringValid(this.report.shortName)) {
      return false;
    }

    if (!isStringValid(this.report.devIndex)) {
      return false;
    }

    if (this.report.devIndex.length !== 6) {
      return false;
    }

    if (!isStringValid(this.report.devSubject)) {
      return false;
    }

    if (!isINNValid(this.report.INN)) {
      this.showInn2Error = true;
      return false;
    }

    return true;
  }

  public showErrorModal() {
    this.showErrors = true;
    this.modalService.show(GeneralInformationErrorComponent, { class: 'modal-lg', initialState: { showGeneralInformationError: true } });
  }

}
