import { Component, Input, OnInit } from '@angular/core';
import * as _ from 'lodash';
import * as moment from 'moment';

import { Report, ObligationsRefItem, OtherCostsRefItem } from '../models/report.model';
import { ErrorComponent } from 'src/app/modals/error.component';
import { BsModalService } from 'ngx-bootstrap';
import { OverdueObligationsComponent } from '../modals/overdue-obligations.component';
import { OtherCostsComponent } from '../modals/other-costs.component';

@Component({
  selector: 'app-step-reference',
  templateUrl: './reference.component.html',
})
export class ReferenceComponent implements OnInit {
  @Input() report: Report = new Report();

  loading = false;
  showErrors = false;
  showOtherCostsError = false;
  showObligationsRefError = false;

  hasNoOverdueObligations = false;
  hasNoOtherCosts = false;

  constructor(private modalService: BsModalService) {
  }

  ngOnInit() {
    if (!this.report.obligationsRef) {
      this.report.obligationsRef = {
        items: [],
      };
      this.report.obligationsRefCount = 0;
    }

    if (_.isArray(this.report.obligationsRef)) {
      this.report.obligationsRef = {
        items: this.report.obligationsRef,
      };
      this.report.obligationsRefCount = this.report.obligationsRef.items.length;
    }

    if (!this.report.otherCostsRef) {
      this.report.otherCostsRef = {
        items: [],
      };
      this.report.otherCostsRefCount = 0;
    }

    if (_.isArray(this.report.otherCostsRef)) {
      this.report.otherCostsRef = {
        items: this.report.otherCostsRef,
      };
      this.report.otherCostsRefCount = this.report.otherCostsRef.items.length;
    }
  }

  onChangeHasNoOverdueObligations(newVal) {
    if (newVal === true) {
      this.report.obligationsRefCount = 0;
      this.report.obligationsRef.items = [];
    } else {
      this.report.obligationsRefCount = this.report.obligationsRef.items.length;
    }
  }

  onChangeHasNoOtherCosts(newVal) {
    if (newVal === true) {
      this.report.otherCostsRefCount = 0;
      this.report.otherCostsRef.items = [];
    } else {
      this.report.otherCostsRefCount = this.report.otherCostsRef.items.length;
    }
  }

  addObligationsRef() {
    const initialState = {
      obligationsRef: new ObligationsRefItem(),
    };
    const modal = this.modalService.show(OverdueObligationsComponent, { class: 'modal-lg', initialState });

    modal.content.event.subscribe((obligationsRef) => {
      // @ts-ignore
      this.report.obligationsRef.items.push(obligationsRef);
      this.report.obligationsRefCount = this.report.obligationsRef.items.length;
    });
  }

  removeObligationsRef(index) {
    // @ts-ignore
    this.report.obligationsRef.items.splice(index, 1);
    this.report.obligationsRefCount = this.report.obligationsRef.items.length;
  }

  editObligationsRef(index) {
    const initialState = {
      obligationsRef: JSON.parse(JSON.stringify(this.report.obligationsRef.items[index])),
    };
    const modal = this.modalService.show(OverdueObligationsComponent, { class: 'modal-lg', initialState });

    modal.content.event.subscribe((obligationsRef) => {
      this.report.obligationsRef.items[index] = obligationsRef;
    });
  }


  addOtherCosts() {
    const initialState = {
      otherCostsItem: new OtherCostsRefItem(),
    };
    const modal = this.modalService.show(OtherCostsComponent, { class: 'modal-lg', initialState });

    modal.content.event.subscribe((otherCostsItem) => {
      // @ts-ignore
      this.report.otherCostsRef.items.push(otherCostsItem);
      this.report.otherCostsRefCount = this.report.otherCostsRef.items.length;
    });
  }

  removeOtherCosts(index) {
    // @ts-ignore
    this.report.otherCostsRef.items.splice(index, 1);
    this.report.otherCostsRefCount = this.report.otherCostsRef.items.length;
  }

  editOtherCosts(index) {
    const initialState = {
      otherCostsItem: JSON.parse(JSON.stringify(this.report.otherCostsRef.items[index])),
    };
    const modal = this.modalService.show(OtherCostsComponent, { class: 'modal-lg', initialState });

    modal.content.event.subscribe((otherCostsItem) => {
      this.report.otherCostsRef.items[index] = otherCostsItem;
    });
  }


  public showError() {
    this.modalService.show(ErrorComponent, {
      initialState: {
        title: `
          <span class="mandatory">Некорректно заполнены обязательные поля.</span><br/>
          Ознакомьтесь с правилами заполнения данных формы:
          `,
        message: `
            <h3>Раздел "3. Справки"</h3>            
            <b>Справка о наличии просроченных обязательств по договорам</b>
            <ul>
              <li>
                При наличии неисполненных (просроченных) обязательств по договорам предоставляется справка с указанием причин ненадлежащего исполнения обязательств. 
                Количество справок (строк) должно соответствовать количеству форм II. «Сведения о многоквартирном доме и (или) ином объекте недвижимости, строящемся 
                (создаваемом) застройщиком с привлечением денежных средств участников долевого строительства», по которым имеются просроченные обязательства
              </li>
            </ul>

            <b>Справка о целевом расходовании денежных средств</b>
            <ul>
              <li>
                Если в форме II. «Сведения о многоквартирном доме и (или) ином объекте недвижимости, строящемся (создаваемом) застройщиком с привлечением денежных 
                средств участников долевого строительства» в пункте 12.4 (сумма денежных средств участников долевого строительства (млн. рублей), использованных 
                застройщиком в отчетном периоде по целевому назначению») указано значение, отличное от 0, то необходимо предоставить справку о целевом расходовании 
                денежных средств. Сумма всех значений, указанных в разделе «Справка о составе целевых расходов», должна соответствовать сумме, указанной в пункте 12.4.
              </li>
            </ul>
          `,
      }
    });
  }

  public isValid() {
    this.showObligationsRefError = false;
    this.showOtherCostsError = false;
    this.showErrors = false;

    const year = +this.report.year;
    const quarter = +this.report.ikvartal;
    const quarterRanges = this.getQuarterRangesForYear(year);
    const currentQuarterRanges = quarterRanges[quarter - 1].map((e) => {
      return moment(`${year}-${e.month() + 1}-${e.date()}`, 'YYYY-MM-DD');
    });

    const unusedContracts = this.report.Appendix2s.filter((appendix) => {
      const timeTransfer = !!appendix.timeTransfer && moment(appendix.timeTransfer, 'YYYY-MM-DD');
      const amountUnUsedContract = parseFloat(appendix.amountUnUsedContract.toString());

      return !!(timeTransfer.isSameOrBefore(currentQuarterRanges[1]) && amountUnUsedContract > 0);
    });

    if (unusedContracts.length !== this.report.obligationsRef.items.length) {
      this.showObligationsRefError = true;
    }

    let totalCashCompensation = 0;
    let totalOtherCostsSum = 0;

    this.report.Appendix2s.forEach((appendix) => {
      totalCashCompensation += +appendix.totalCashCompensation * 1000;
    });

    this.report.otherCostsRef.items.forEach((item) => {
      totalOtherCostsSum += +item.sum;
    });

    if (totalOtherCostsSum !== totalCashCompensation) {
      this.showOtherCostsError = true;
    }

    if (this.showObligationsRefError || this.showOtherCostsError) {
      this.showErrors = true;
      return false;
    }

    return true;
  }

  getQuarterRangesForYear(year) {
    const yearToCheck = year || moment().year();

    return [
      [moment(yearToCheck + '-01-01', 'YYYY-MM-DD'), moment(yearToCheck + '-03-31', 'YYYY-MM-DD')],
      [moment(yearToCheck + '-04-01', 'YYYY-MM-DD'), moment(yearToCheck + '-06-30', 'YYYY-MM-DD')],
      [moment(yearToCheck + '-07-01', 'YYYY-MM-DD'), moment(yearToCheck + '-09-30', 'YYYY-MM-DD')],
      [moment(yearToCheck + '-10-01', 'YYYY-MM-DD'), moment(yearToCheck + '-12-31', 'YYYY-MM-DD')]
    ];
  }
}
