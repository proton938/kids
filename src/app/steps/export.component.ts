import { Component, Input, OnInit, Injectable } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { HttpClient } from '@angular/common/http';
import { v4 as uuidv4 } from 'uuid';
import { saveAs } from 'file-saver';
import { ReportService} from "../models/report.service";
import { Report } from '../models/report.model';

@Injectable()

@Component({
  selector: 'app-step-export',
  templateUrl: './export.component.html',
})
export class ExportComponent implements OnInit {
  @Input() report: Report;

  constructor(private modalService: BsModalService, private httpService: HttpClient, private reportService: ReportService) {
  }

  fileName: string;

  ngOnInit() {

    this.fileName = uuidv4();
  }


  exportXML() {
    // const formData: FormData = new FormData();
    // formData.append('json', JSON.stringify(this.report));

    const formData = {
      json: this.report,
    };

    this.httpService.post('https://smart-dev.reinform-int.ru/dol/api/render_xml/', formData, { responseType: 'text' }).subscribe(
      (XMLText) => {

        let parser = new DOMParser();
        let XMLCorrect: any = parser.parseFromString(XMLText, "text/xml");
        XMLCorrect.getElementsByTagName("regDate")[0].childNodes[0].nodeValue = XMLCorrect.getElementsByTagName("regDate")[0].childNodes[0].nodeValue.slice(0, 10);
        XMLCorrect.getElementsByTagName("seaIsOrganization")[0].childNodes[0].nodeValue = this.report.seaIsOrganization;

        let Appendix2 = XMLCorrect.querySelector("infBorrowing").parentNode;

        let creditOrganizations = XMLCorrect.createElement("creditOrganizations");
        for (let i = 0; i < this.reportService.serviceAppendix2[0].creditOrganizations.length; i++) {
          if (this.reportService.serviceAppendix2[0].creditOrganizations[i]) {
            let creditOrganization = XMLCorrect.createElement("creditOrganization");
            for (let key in this.reportService.serviceAppendix2[0].creditOrganizations[i]) {
              let childElement = XMLCorrect.createElement(key);
              let childElementText = XMLCorrect.createTextNode(this.reportService.serviceAppendix2[0].creditOrganizations[i][key]);
              childElement.appendChild(childElementText);
              creditOrganization.appendChild(childElement);
            }
            creditOrganizations.appendChild(creditOrganization);
          }
        }
        Appendix2.appendChild(creditOrganizations);
        console.log(this.reportService.serviceAppendix2[0]);

        let loanCompanies = XMLCorrect.createElement("loanCompanies");
        for (let i = 0; i < this.reportService.serviceAppendix2[0].loanCompanies.length; i++) {
          if (this.reportService.serviceAppendix2[0].loanCompanies[i]) {
            let loanCompany = XMLCorrect.createElement("loanCompany");
            for (let key in this.reportService.serviceAppendix2[0].loanCompanies[i]) {
              let childElement = XMLCorrect.createElement(key);
              let childElementText = XMLCorrect.createTextNode(this.reportService.serviceAppendix2[0].loanCompanies[i][key]);
              childElement.appendChild(childElementText);
              loanCompany.appendChild(childElement);
            }
            loanCompanies.appendChild(loanCompany);
          }
        }
        Appendix2.appendChild(loanCompanies);
        console.log(this.reportService.serviceAppendix2[0]);

        let exportText = (new XMLSerializer()).serializeToString(XMLCorrect);
        const blob2 = new Blob([exportText], { type: 'text/xml;charset=utf-8' });
        saveAs(blob2, `${this.fileName}.xml`);
      },
      (error) => {
        console.log('error', error);
      }
    );
  }

  exportPDF() {
    // const formData: FormData = new FormData();
    // formData.append('json', JSON.stringify(this.report));

    const formData = {
      json: JSON.stringify(this.report),
    };

    this.httpService.post('https://smart-dev.reinform-int.ru/dol/api/render_pdf/', formData, { responseType: 'text' }).subscribe(
      (XMLText) => {
        const blob2 = new Blob([XMLText], { type: 'text/pdf;charset=utf-8' });
        saveAs(blob2, `${this.fileName}.pdf`);
      },
      (error) => {
        console.log('error', error);
      }
    );
  }
}
