import { Component, Input, ViewChild, OnInit, OnChanges } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BsModalService } from 'ngx-bootstrap';
import * as _ from 'lodash';

import { Report } from '../models/report.model';
import { ErrorComponent } from 'src/app/modals/error.component';

@Component({
  selector: 'app-step-balance',
  templateUrl: './balance.component.html',
})
export class BalanceComponent {
  @Input() report: Report = new Report();

  loading = false;
  showErrors = false;

  @ViewChild('loadedReport', null) loadedReport;

  constructor(private modalService: BsModalService, private httpService: HttpClient) {
  }

  clear() {
    this.report.balanceNodes = {
      assets: undefined,
      liabilities: undefined,
      report: undefined,
    };

    this.report.buildingOutsideOfMoscow = 'false';
    this.report.h1 = undefined;
    this.report.h2 = undefined;

    this.report.pf = undefined;
    this.report.normPf = undefined;
    this.report.b1600 = undefined;
    this.report.b16000 = undefined;
    this.report.measuringUnit = 'RUBLES';

    this.report.withoutUnfinishedBuildingActivities = false;
    this.report.withoutContractDebts = false;
    this.report.withoutContractLiabilities = false;
    this.report.withoutDebtOfFounders = false;
    this.report.withoutRevenueOfFuturePeriods = false;

    this.report.withoutUnfinishedBuildingActivities673 = false;
    this.report.withoutTaxesAndFees673 = false;
    this.report.withoutFinishedBuildingActivities673 = false;
    this.report.withoutPrepayment673 = false;
    this.report.withoutOtherCosts673 = false;
    this.report.withoutContractDebts673 = false;
    this.report.withoutValueAddedTax673 = false;
    this.report.withoutCashInBankAccounts673 = false;
    this.report.withoutRevenueOfFuturePeriods673 = false;
    this.report.withoutDeferredTaxLiabilities673 = false;
  }

  load() {
    this.loadedReport.nativeElement.click();
  }

  handleFileInput(files) {
    const formData: FormData = new FormData();
    const xls = files.item(0);

    formData.append('xls', xls, xls.name);
    formData.append('quarter', this.report.ikvartal.toString());
    formData.append('year', this.report.year);
    formData.append('INN', this.report.INN);
    formData.append('voluntary', 'N');

    this.loading = true;

    this.httpService.post('https://smart-dev.reinform-int.ru/dol/api/import_xls/', formData).subscribe(
      (result: any) => {
        // this.report = result.Report;
        // result.message
        if (result.assets) {
          this.report.balanceNodes.assets = result.assets;
        } else if (result.liabilities) {
          this.report.balanceNodes.liabilities = result.liabilities;
        } else if (result.report) {
          this.report.balanceNodes.report = result.report;
        } else {
          this.modalService.show(ErrorComponent, {
            initialState: {
              title: 'Ошибка при загрузке файлов',
              message: result.message,
            }
          });
        }
        console.log('json', JSON.stringify(result));
        console.log('result', result);
        this.loading = false;
      },
      (error) => {
        console.log('error', error);
        this.loading = false;
      }
    );
  }

  public isValid() {
    if (!this.report.balanceNodes.assets) {
      return false;
    }

    if (!this.report.balanceNodes.liabilities) {
      return false;
    }

    if (!this.report.balanceNodes.report) {
      return false;
    }

    return true;
  }
}
