import { Component, Input, OnInit } from '@angular/core';
import * as _ from 'lodash';
import { HttpClient } from '@angular/common/http';
import { Report, ConsolidatedCumulativeSheet } from '../models/report.model';
import { BsModalService } from 'ngx-bootstrap';
import { ConsolidatedCumulativeSheetsComponent } from '../modals/consolidated-cumulative-sheets.component';
import {ReportService} from "../models/report.service";

@Component({
  selector: 'app-step-statement',
  templateUrl: './statement.component.html',
})
export class StatementComponent implements OnInit {
  @Input() report: Report = new Report();

  loading = false;
  showErrors = false;

  constructor(private modalService: BsModalService, private httpService: HttpClient, private reportService: ReportService) {
  }

  buildingPermits: any[] = [];
  actualBP = [];

  loadConsolidatedCumulativeSheets() {
    const usedBuildingPermitNumber = _.map(this.report.consolidatedCumulativeSheets, (sheet) => {
      return sheet.SheetBuildingPermit.permitNumber;
    });
    const initialState = {
      consolidatedCumulativeSheet: new ConsolidatedCumulativeSheet(),
      buildingPermits: this.buildingPermits.filter((buildingPermit) => {
        return !usedBuildingPermitNumber.includes(buildingPermit.permitNumber);
      }),
    };
    if (initialState.buildingPermits.length > 0) {
      for (let i=0; i<initialState.buildingPermits.length; i++) {
        this.loadArray.SheetBuildingPermit.description = this.report['ConsolidatedCumulativeSheets'][0]['SheetBuildingPermit']['description'];
        this.loadArray.SheetBuildingPermit.periodOfValidity = this.report['ConsolidatedCumulativeSheets'][0]['SheetBuildingPermit']['periodOfValidity'];
        this.loadArray.SheetBuildingPermit.permitDate = this.report['ConsolidatedCumulativeSheets'][0]['SheetBuildingPermit']['permitDate'];
        this.loadArray.SheetBuildingPermit.permitDistributor = this.report['ConsolidatedCumulativeSheets'][0]['SheetBuildingPermit']['permitDistributor'];
        this.loadArray.SheetBuildingPermit.permitNumber = this.report['ConsolidatedCumulativeSheets'][0]['SheetBuildingPermit']['permitNumber'];
        this.loadArray.sheetBuildingProject = this.report['ConsolidatedCumulativeSheets'][0]['sheetBuildingProject'];
        this.loadArray.sheetBuildingAddress = this.report['ConsolidatedCumulativeSheets'][0]['sheetBuildingAddress'];
        this.loadArray.sheetBuildingArea = this.report['ConsolidatedCumulativeSheets'][0]['sheetBuildingArea'];
        this.loadArray.sheetConstructionTime = this.report['ConsolidatedCumulativeSheets'][0]['sheetConstructionTime'];
      }
      this.report.consolidatedCumulativeSheets.push(this.loadArray);
    }
  }

  loadArray: any = {
    "SheetBuildingPermit": {
      "description": "",
      "periodOfValidity": "",
      "permitDate": "",
      "permitDistributor": "",
      "permitNumber": ""
    },
    "sheetBuildingProject": "",
    "sheetBuildingAddress": "",
    "sheetBuildingArea": "1",
    "sheetConstructionTime": "1"
  };

  ngOnInit() {
    this.reportService.reportBuffer = this.report['ConsolidatedCumulativeSheets'];
    if (!this.report.consolidatedCumulativeSheets) {
      this.report.consolidatedCumulativeSheets = [];
    }
    this.filterBuildingPermits();
  }


  loadModelForModal() {
    this.reportService.reportBuffer = this.report;
  }

  getStatus(permitNumber) {
    if (this.actualBP.includes(permitNumber)) {
      return 'Актуально';
    }

    if (this.buildingPermits.find((t) => (t.permitNumber === permitNumber))) {
      return 'Не актуально';
    } else {
      return 'Отсутствует';
    }
  }

  filterBuildingPermits() {
    this.actualBP = [];

    this.buildingPermits = _.flatMap(this.report.Appendix2s, (appendix) => {
      return [
        ...appendix.buildingPermits.map((buildingPermit) => {
          return {
            ...buildingPermit,
            buildingProject: appendix.realEstateKind,
            buildingAddress: appendix.placeObject,
          };
        }),
      ];
    });

    this.report.Appendix2s.forEach((appendix) => {
      let actualDate = '';
      let actualNumber = '';
      appendix.buildingPermits.forEach((buildingPermit) => {
        const permitDate = buildingPermit.newDate || buildingPermit.periodOfValidity || '';

        if (permitDate > actualDate) {
          actualDate = permitDate;
          actualNumber = buildingPermit.permitNumber;
        }
      });

      if (!this.actualBP.includes(actualNumber)) {
        this.actualBP.push(actualNumber);
      }
      this.loadConsolidatedCumulativeSheets();
    });
  }

  addConsolidatedCumulativeSheet() {
    this.showErrors = false;
    const usedBuildingPermitNumber = _.map(this.report.consolidatedCumulativeSheets, (sheet) => {
      return sheet.SheetBuildingPermit.permitNumber;
    });
    const initialState = {
      consolidatedCumulativeSheet: new ConsolidatedCumulativeSheet(),
      buildingPermits: this.buildingPermits.filter((buildingPermit) => {
        return !usedBuildingPermitNumber.includes(buildingPermit.permitNumber);
      }),
    };

    const modal = this.modalService.show(ConsolidatedCumulativeSheetsComponent, { class: 'modal-max', initialState });

    modal.content.event.subscribe((consolidatedCumulativeSheet) => {
      // @ts-ignore
      this.report.consolidatedCumulativeSheets.push(consolidatedCumulativeSheet);
    });
  }

  removeConsolidatedCumulativeSheet(index) {
    this.showErrors = false;
    // @ts-ignore
    this.report.consolidatedCumulativeSheets.splice(index, 1);
  }

  editConsolidatedCumulativeSheet(index) {
    this.showErrors = false;
    const usedBuildingPermitNumber = _.map(this.report.consolidatedCumulativeSheets, (sheet) => {
      return sheet.SheetBuildingPermit && sheet.SheetBuildingPermit.permitNumber;
    });
    const initialState = {
      consolidatedCumulativeSheet: JSON.parse(JSON.stringify(this.report.consolidatedCumulativeSheets[index])),
      buildingPermits: this.buildingPermits.filter((buildingPermit) => {
        return !usedBuildingPermitNumber.includes(buildingPermit.permitNumber);
      }),
    };
    const modal = this.modalService.show(ConsolidatedCumulativeSheetsComponent, { class: 'modal-max', initialState });

    modal.content.event.subscribe((consolidatedCumulativeSheet) => {
      this.report.consolidatedCumulativeSheets[index] = consolidatedCumulativeSheet;
    });
  }

  public showError() {
    this.showErrors = true;
  }

  public isValid() {
    this.showErrors = false;

    const bpNumbers = this.report.consolidatedCumulativeSheets
    .map((sheet) => (
      sheet.SheetBuildingPermit && (sheet.SheetBuildingPermit.permitNumber)
    ));

    return _.difference(this.actualBP, bpNumbers).length === 0;
  }
}
