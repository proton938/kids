import { Component, Input, EventEmitter } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';

import { InsuranceAgreement } from 'src/app/models/report.model';
import { isStringValid, isINNValid } from 'src/app/utils/validation.utils';

import { GeneralInformationErrorComponent } from './general-information-error.component';

@Component({
  selector: 'app-insurance-contracts-modal',
  templateUrl: './insurance-contracts.component.html',
})
export class InsuranceContractsComponent {
  @Input() insuranceAgreement: InsuranceAgreement;

  public event: EventEmitter<any> = new EventEmitter();
  showErrors = false;
  showInnError = false;

  constructor(public activeModal: BsModalRef, private modalService: BsModalService) { }

  isValid() {
    if (!isStringValid(this.insuranceAgreement.insuranceOrganizationName)) {
      return false;
    }

    if (isStringValid(this.insuranceAgreement.insuranceOrganizationINN) && !isINNValid(this.insuranceAgreement.insuranceOrganizationINN)) {
      this.showInnError = true;
      return false;
    }

    return true;
  }

  public showErrorModal() {
    this.modalService.show(GeneralInformationErrorComponent, { class: 'modal-lg', initialState: { showAppendix2Error: true } });
  }

  add() {
    if (!this.isValid()) {
      this.showErrors = true;
      // this.showErrorModal();
      return;
    }

    this.activeModal.hide();
    this.event.emit(this.insuranceAgreement);
  }
}
