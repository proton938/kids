import { Component, Input } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap';

@Component({
  selector: 'app-error-modal',
  templateUrl: './error.component.html',
})
export class ErrorComponent {
  @Input() title = '';
  @Input() message = '';

  constructor(public activeModal: BsModalRef) {}
}
