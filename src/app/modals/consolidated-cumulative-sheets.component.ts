import { Component, Input, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { ConsolidatedCumulativeTableComponent } from '../components/consolidated-cumulative-table.component';

import { ConsolidatedCumulativeSheet, BuildingPermit } from '../models/report.model';
import { isStringValid, isLongFloatValid } from '../utils/validation.utils';
import { ErrorComponent } from './error.component';

import { GeneralInformationErrorComponent } from './general-information-error.component';

@Component({
  selector: 'app-consolidated-cumulative-sheets-modal',
  templateUrl: './consolidated-cumulative-sheets.component.html',
})
export class ConsolidatedCumulativeSheetsComponent {
  @Input() consolidatedCumulativeSheet: ConsolidatedCumulativeSheet;
  @Input() buildingPermits: BuildingPermit[];

  @ViewChild('tableData', null) tableData: ConsolidatedCumulativeTableComponent;

  public event: EventEmitter<any> = new EventEmitter();
  showErrors = false;

  constructor(public activeModal: BsModalRef, private modalService: BsModalService) { }

  isValid() {
    let isValid = true;

    if (!this.consolidatedCumulativeSheet.SheetBuildingPermit) {
      isValid = false;
    }

    if (!isStringValid(this.consolidatedCumulativeSheet.sheetBuildingProject)) {
      isValid = false;
    }

    if (!isStringValid(this.consolidatedCumulativeSheet.sheetBuildingAddress)) {
      isValid = false;
    }

    if (!isStringValid(this.consolidatedCumulativeSheet.sheetBuildingArea)) {
      isValid = false;
    }

    if (!isStringValid(this.consolidatedCumulativeSheet.sheetConstructionTime)) {
      isValid = false;
    }

    if (!this.tableData.isValid()) {
      isValid = false;
    }

    return isValid;
  }

  public showErrorModal() {
    this.modalService.show(GeneralInformationErrorComponent, { class: 'modal-lg', initialState: { showAppendix2Error: true } });
  }

  onChangeSheetBuildingPermit(a) {
    if (!this.consolidatedCumulativeSheet.sheetBuildingProject || this.consolidatedCumulativeSheet.sheetBuildingProject === '') {
      this.consolidatedCumulativeSheet.sheetBuildingProject = a.buildingProject;
    }

    if (!this.consolidatedCumulativeSheet.sheetBuildingAddress || this.consolidatedCumulativeSheet.sheetBuildingAddress === '') {
      this.consolidatedCumulativeSheet.sheetBuildingAddress = a.buildingAddress;
    }

    /// @ts-ignore
    delete this.consolidatedCumulativeSheet.SheetBuildingPermit.buildingProject;
    /// @ts-ignore
    delete this.consolidatedCumulativeSheet.SheetBuildingPermit.buildingAddress;
  }

  add() {
    if (!this.isValid()) {
      this.showErrors = true;
      this.modalService.show(ErrorComponent, {
        initialState: {
          title: `
            <span class="mandatory">Некорректно заполнены обязательные поля.</span><br/>
            Ознакомьтесь с правилами заполнения данных формы:
            `,
          message: `
              <h3>Раздел "5. Ведомость"</h3>
              <ul>
              <li>Обязательные к заполнению поля отмечены <span class="mandatory">*</span></li>
              <li>При попытке сохранения или перехода на следующий шаг неверно заполненные поля подсвечиваются красным.</li>
              <li>Поля, содержащие числовые значения в виде денежных единиц, заполняются целочисленными значениями, 
              содержащими минимум 2 знака после разделителя (100.00).</li>
              </ul>
            `,
        }
      });
      return;
    }

    this.activeModal.hide();
    this.event.emit(this.consolidatedCumulativeSheet);
  }
}
