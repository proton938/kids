import { Component, Input, OnInit, EventEmitter } from '@angular/core';
import { BuildingPermit, Appendix2, InsuranceAgreement, CreditOrganization, LoanCompany, ScheduleWorkItem } from 'src/app/models/report.model';
import { CreditOrganizationComponent } from './credit-organization.component';
import { LoanCompanyComponent } from './loan-company.component';
import { InsuranceContractsComponent } from './insurance-contracts.component';
import { BuildingPermitsComponent } from './building-permits.component';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { GeneralInformationErrorComponent } from './general-information-error.component';
import { format, add } from 'mathjs';
import { isStringValid, isNotEmptyArray, isINNValid } from 'src/app/utils/validation.utils';
import * as $ from 'jquery';
import Inputmask from 'inputmask';
import { ReportService } from "../models/report.service";

@Component({
  selector: 'app-information-about-the-property-modal',
  templateUrl: './information-about-the-property.component.html',
})
export class InformationAboutThePropertyComponent implements OnInit {

  constructor(public activeModal: BsModalRef, private modalService: BsModalService, private reportSerice: ReportService) {
  }
  @Input() appendix2: Appendix2;
  @Input() rightKinds: any[];
  @Input() innRnsMap: any = {};
  @Input() inn: string;
  @Input() ikvartal: string;
  @Input() year: string;

  public event: EventEmitter<any> = new EventEmitter();

  showErrors = false;
  showInnError = false;

  isNewRules: boolean;
  RNSList: string[] = [];

  isAmountUnUsedContractChangedByUser = false;

  isTotalAmountOutstandingChangedByUser = false;

  ngOnInit() {
    this.isNewRules = new Date(this.appendix2.startDate).getTime() > new Date('01.01.2014').getTime();
    if (this.innRnsMap[this.inn] !== undefined) {
      this.RNSList = this.innRnsMap[this.inn];
    }
    if (this.appendix2.cadastralNumber.length === 0) {
      this.addCadastralNumber();
    }

    this.checkEmptyCadastralNumber();
  }

  startDateChanged() {
    this.isNewRules = new Date(this.appendix2.startDate).getTime() > new Date('01.01.2014').getTime();
  }

  calcNumberOfIndParts() {
    this.appendix2.numberOfIndParts = 0;
    this.appendix2.numberOfIndParts += +this.appendix2.numberOfLivingQuarters || 0;
    this.appendix2.numberOfIndParts += +this.appendix2.numberOfNonLivingQuarters || 0;
    this.appendix2.numberOfIndParts += +this.appendix2.numberOfCarPark || 0;
  }

  calcAreaOfIndParts() {
    this.appendix2.areaOfIndParts = add(
      0,
      +this.appendix2.areaOfLivingQuarters || 0,
      +this.appendix2.areaOfNonLivingQuarters || 0,
      +this.appendix2.areaOfCarPark || 0
    );

    this.appendix2.areaOfIndParts = format(this.appendix2.areaOfIndParts, { precision: 14 });
  }

  onChangeBorrowing(newValue) {
    if (newValue === 'false') {
      this.appendix2.creditOrganizations = [];
      this.appendix2.loanCompanies = [];
    }
  }

  onChangeContractsToReportDate(newValue) {
    if (!this.isAmountUnUsedContractChangedByUser) {
      this.appendix2.amountUnUsedContract = newValue;
    }
  }

  onChangePermissionCommision(newValue) {
    if (newValue === 'false') {
      this.appendix2.partiallyEntered = 'false';
      this.appendix2.permCommissionNumber = '';
      this.appendix2.permCommissionDate = null;
      this.appendix2.permCommissionDistributor = '';
    }
  }

  onChangeInsuranceAgreementAbsent(newValue) {
    if (newValue === true) {
      this.appendix2.InsuranceAgreements = [];
    }
  }

  onChangeBankGuarantorAbsent(newValue) {
    if (newValue === true) {
      this.appendix2.bankGuarantorName = '';
      this.appendix2.bankGuarantorINN = '';
      this.appendix2.bankGuarantorPlace = '';
      this.appendix2.contractGuaranteeDate = null;
      this.appendix2.contractGuaranteeNumber = '';
      this.appendix2.contractGuaranteeEndDate = null;
      this.appendix2.contractGuaranteeNewDate = null;
    }
  }

  onUserChangeAmountUnUsedContract() {
    this.isAmountUnUsedContractChangedByUser = true;
  }

  onUserChangeTotalAmountOutstanding() {
    this.isTotalAmountOutstandingChangedByUser = true;
  }

  onChangeSumObligation(newValue) {
    if (!this.isTotalAmountOutstandingChangedByUser) {
      this.appendix2.totalAmountOutstanding = newValue;
    }
  }

  checkEmptyCadastralNumber() {
    const hasEmptyField = this.appendix2.cadastralNumber.some((cadastralNumber) => {
      return cadastralNumber === '';
    });

    if (!hasEmptyField) {
      this.addCadastralNumber();
    }
  }

  onPasteCadastralNumber(i, event) {
    // @ts-ignore
    const clipboardData = event.clipboardData || window.clipboardData;
    const pastedText = clipboardData.getData('text');
    this.appendix2.cadastralNumber[i] = pastedText;

    this.checkEmptyCadastralNumber();
  }

  validateLongFloat(event) {
    if (event.target.value !== '') {
      event.target.value = parseFloat(event.target.value).toFixed(2);
    }
  }

  addCadastralNumber() {
    this.appendix2.cadastralNumber.push('');
    setTimeout(() => {
      Inputmask().mask('.input-cadastral-number');
    });
  }

  trackCadastralNumber(idx) {
    return idx;
  }

  removeCadastralNumber(index) {
    this.appendix2.cadastralNumber.splice(index, 1);
  }

  addBuildingPermit() {
    const initialState = {
      buildingPermit: new BuildingPermit(),
      RNSList: this.RNSList,
    };

    const modal = this.modalService.show(BuildingPermitsComponent, { class: 'modal-lg child-modal', initialState });
    // this.activeModal.setClass('hide');

    modal.content.event.subscribe((buildingPermit) => {
      this.appendix2.buildingPermits.push(buildingPermit);
    });
  }

  editBuildingPermit(index) {
    const initialState = {
      buildingPermit: JSON.parse(JSON.stringify(this.appendix2.buildingPermits[index])),
      RNSList: this.RNSList,
    };
    const modal = this.modalService.show(BuildingPermitsComponent, { class: 'modal-lg child-modal', initialState });

    modal.content.event.subscribe((buildingPermit) => {
      this.appendix2.buildingPermits[index] = buildingPermit;
    });
  }

  removeBuildingPermit(index) {
    this.appendix2.buildingPermits.splice(index, 1);
  }

  addCreditOrganization() {
    const initialState = {
      creditOrganization: new CreditOrganization(),
    };

    const modal = this.modalService.show(CreditOrganizationComponent, { class: 'modal-lg child-modal', initialState });
    // this.activeModal.setClass('hide');

    modal.content.event.subscribe((creditOrganization) => {
      this.appendix2.creditOrganizations.push(creditOrganization);
    });
  }

  editCreditOrganization(index) {
    const initialState = {
      creditOrganization: JSON.parse(JSON.stringify(this.appendix2.creditOrganizations[index])),
    };
    const modal = this.modalService.show(CreditOrganizationComponent, { class: 'modal-lg child-modal', initialState });

    modal.content.event.subscribe((creditOrganization) => {
      this.appendix2.creditOrganizations[index] = creditOrganization;
    });
  }

  removeCreditOrganization(index) {
    this.appendix2.creditOrganizations.splice(index, 1);
  }

  addLoanCompany() {
    const initialState = {
      loanCompany: new LoanCompany(),
    };

    const modal = this.modalService.show(LoanCompanyComponent, { class: 'modal-lg child-modal', initialState });
    // this.activeModal.setClass('hide');

    modal.content.event.subscribe((loanCompany) => {
      this.appendix2.loanCompanies.push(loanCompany);
    });
  }

  editLoanCompany(index) {
    const initialState = {
      loanCompany: JSON.parse(JSON.stringify(this.appendix2.loanCompanies[index])),
    };
    const modal = this.modalService.show(LoanCompanyComponent, { class: 'modal-lg child-modal', initialState });

    modal.content.event.subscribe((loanCompany) => {
      this.appendix2.loanCompanies[index] = loanCompany;
    });
  }

  removeLoanCompany(index) {
    this.appendix2.loanCompanies.splice(index, 1);
  }

  addInsuranceContract() {
    const initialState = {
      insuranceAgreement: new InsuranceAgreement(),
    };

    const modal = this.modalService.show(InsuranceContractsComponent, { class: 'modal-lg child-modal', initialState });
    // this.activeModal.setClass('hide');

    modal.content.event.subscribe((insuranceAgreement) => {
      this.appendix2.InsuranceAgreements.push(insuranceAgreement);
    });
  }

  editInsuranceContract(index) {
    const initialState = {
      insuranceAgreement: JSON.parse(JSON.stringify(this.appendix2.InsuranceAgreements[index])),
    };

    const modal = this.modalService.show(InsuranceContractsComponent, { class: 'modal-lg child-modal', initialState });
    // this.activeModal.setClass('hide');

    modal.content.event.subscribe((insuranceAgreement) => {
      this.appendix2.InsuranceAgreements[index] = insuranceAgreement;
    });
  }

  removeInsuranceContract(index) {
    this.appendix2.InsuranceAgreements.splice(index, 1);
  }

  public isValid() {
    const appendix2s = this.appendix2;

    if (!isStringValid(appendix2s.realEstateKind)) {
      console.log('if (!isStringValid(appendix2s.realEstateKind)) {');
      return false;
    }

    if (!isStringValid(appendix2s.placeObject)) {
      console.log('if (!isStringValid(appendix2s.placeObject)) {');
      return false;
    }

    const cadastralNumbers = appendix2s.cadastralNumber.slice(0, -1);

    if (!isNotEmptyArray(cadastralNumbers)) {
      console.log('if (!isNotEmptyArray(appendix2s.cadastralNumber)) {');
      return false;
    }

    if (cadastralNumbers.some((cadastralNumber) => (!/\d{1,2}:\d{1,2}:\d{1,7}(:\d{0,7})?/.test(cadastralNumber)))) {
      return false;
    }

    if (!appendix2s.rightKind) {
      console.log('    if (!appendix2s.rightKind) {');
      return false;
    }

    if (appendix2s.rightKind !== 'Собственность') {
      if (!isStringValid(appendix2s.contractNumber)) {
        console.log('if (!isStringValid(appendix2s.contractNumber)) {');
        return false;
      }

      if (!appendix2s.contractDate) {
        console.log('if (!appendix2s.contractDate) {');
        return false;
      }
    }

    if (!isStringValid(appendix2s.numberOfLivingQuarters)) {
      console.log('if (!isStringValid(appendix2s.numberOfLivingQuarters)) {');
      return false;
    }

    if (!isStringValid(appendix2s.areaOfLivingQuarters)) {
      console.log('if (!isStringValid(appendix2s.areaOfLivingQuarters)) {');
      return false;
    }

    if (!isStringValid(appendix2s.numberOfNonLivingQuarters)) {
      console.log('if (!isStringValid(appendix2s.numberOfNonLivingQuarters)) {');
      return false;
    }

    if (!isStringValid(appendix2s.areaOfNonLivingQuarters)) {
      console.log('if (!isStringValid(appendix2s.areaOfNonLivingQuarters)) {');
      return false;
    }

    if (!isStringValid(appendix2s.numberOfCarPark)) {
      console.log('if (!isStringValid(appendix2s.numberOfCarPark)) {');
      return false;
    }

    if (!isStringValid(appendix2s.areaOfCarPark)) {
      console.log('if (!isStringValid(appendix2s.areaOfCarPark)) {');
      return false;
    }

    if (!isStringValid(appendix2s.numberOfIndParts)) {
      console.log('if (!isStringValid(appendix2s.numberOfIndParts)) {');
      return false;
    }

    if (!isStringValid(appendix2s.areaOfIndParts)) {
      console.log('if (!isStringValid(appendix2s.areaOfIndParts)) {');
      return false;
    }

    if (!isStringValid(appendix2s.estimatedCost)) {
      console.log('if (!isStringValid(appendix2s.estimatedCost)) {');
      return false;
    }

    if (!isStringValid(appendix2s.projectCost)) {
      console.log('if (!isStringValid(appendix2s.projectCost)) {');
      return false;
    }

    if (!isStringValid(appendix2s.reportCost)) {
      console.log('if (!isStringValid(appendix2s.reportCost)) {');
      return false;
    }

    if (!isStringValid(appendix2s.sumCost)) {
      console.log('if (!isStringValid(appendix2s.sumCost)) {');
      return false;
    }

    if (!isStringValid(appendix2s.totalCost)) {
      console.log('if (!isStringValid(appendix2s.totalCost)) {');
      return false;
    }

    if (!isStringValid(appendix2s.contractorsCost)) {
      console.log('if (!isStringValid(appendix2s.contractorsCost)) {');
      return false;
    }

    if (!isStringValid(appendix2s.developerCost)) {
      console.log('if (!isStringValid(appendix2s.developerCost)) {');
      return false;
    }

    if (!isStringValid(appendix2s.developerCost)) {
      console.log('if (!isStringValid(appendix2s.developerCost)) {');
      return false;
    }

    if (appendix2s.infBorrowing === 'true') {
      if (!isNotEmptyArray(appendix2s.creditOrganizations)) {
        return false;
      }
    }

    if (!isStringValid(appendix2s.balanceOfBorrowed)) {
      console.log('if (!isStringValid(appendix2s.balanceOfBorrowed)) {');
      return false;
    }

    if (!isStringValid(appendix2s.amountMoneyParticipants)) {
      console.log('if (!isStringValid(appendix2s.amountMoneyParticipants)) {');
      return false;
    }

    if (!isStringValid(appendix2s.totalAmountCash)) {
      console.log('if (!isStringValid(appendix2s.totalAmountCash)) {');
      return false;
    }

    if (!isStringValid(appendix2s.totalCashCompensation)) {
      console.log('if (!isStringValid(appendix2s.totalCashCompensation)) {');
      return false;
    }

    if (!isStringValid(appendix2s.totalCashCompensationFirstDate)) {
      console.log('if (!isStringValid(appendix2s.totalCashCompensationFirstDate)) {');
      return false;
    }

    if (!isStringValid(appendix2s.totalCashReturnedFirstDate)) {
      console.log('if (!isStringValid(appendix2s.totalCashReturnedFirstDate)) {');
      return false;
    }

    if (!isStringValid(appendix2s.totalCashReturned)) {
      console.log('if (!isStringValid(appendix2s.totalCashReturned)) {');
      return false;
    }

    if (!isStringValid(appendix2s.balance)) {
      console.log('if (!isStringValid(appendix2s.balance)) {');
      return false;
    }

    if (!isStringValid(appendix2s.startDate)) {
      console.log('if (!isStringValid(appendix2s.startDate)) {');
      return false;
    }

    if (!isStringValid(appendix2s.amountContractsConcludedDuringRepPer)) {
      console.log('if (!isStringValid(appendix2s.amountContractsConcludedDuringRepPer)) {');
      return false;
    }

    if (!isStringValid(appendix2s.amountContractsDivorcedDuringRepPer)) {
      console.log('if (!isStringValid(appendix2s.amountContractsDivorcedDuringRepPer)) {');
      return false;
    }

    if (!isStringValid(appendix2s.contractsToReportDate)) {
      console.log('if (!isStringValid(appendix2s.contractsToReportDate)) {');
      return false;
    }

    if (!isStringValid(appendix2s.livingObjectsToReportDate)) {
      console.log('if (!isStringValid(appendix2s.livingObjectsToReportDate)) {');
      return false;
    }

    if (!isStringValid(appendix2s.areaLivingObjectsToReportDate)) {
      console.log('if (!isStringValid(appendix2s.areaLivingObjectsToReportDate)) {');
      return false;
    }

    if (!isStringValid(appendix2s.nonLivingObjectsToReportDate)) {
      console.log('if (!isStringValid(appendix2s.nonLivingObjectsToReportDate)) {');
      return false;
    }

    if (!isStringValid(appendix2s.areaNonLivingObjectsToReportDate)) {
      console.log('if (!isStringValid(appendix2s.areaNonLivingObjectsToReportDate)) {');
      return false;
    }

    if (!isStringValid(appendix2s.numberOfCarParkToReportDate)) {
      console.log('if (!isStringValid(appendix2s.numberOfCarParkToReportDate)) {');
      return false;
    }

    if (!isStringValid(appendix2s.areaOfCarParkToReportDate)) {
      console.log('if (!isStringValid(appendix2s.areaOfCarParkToReportDate)) {');
      return false;
    }

    if (!isStringValid(appendix2s.sumObligation)) {
      console.log('if (!isStringValid(appendix2s.sumObligation)) {');
      return false;
    }

    if (!isStringValid(appendix2s.sumObligationDdu)) {
      console.log('if (!isStringValid(appendix2s.sumObligationDdu)) {');
      return false;
    }

    if (!isStringValid(appendix2s.accountsReceivable)) {
      console.log('if (!isStringValid(appendix2s.accountsReceivable)) {');
      return false;
    }

    if (!isStringValid(appendix2s.totalAmountAttracted)) {
      console.log('if (!isStringValid(appendix2s.totalAmountAttracted)) {');
      return false;
    }

    if (!isStringValid(appendix2s.amountUsedContract)) {
      console.log('if (!isStringValid(appendix2s.amountUsedContract)) {');
      return false;
    }

    if (!isStringValid(appendix2s.amountUnUsedContract)) {
      console.log('if (!isStringValid(appendix2s.amountUnUsedContract)) {');
      return false;
    }

    if (!isStringValid(appendix2s.totalAmountOutstanding)) {
      console.log('if (!isStringValid(appendix2s.totalAmountOutstanding)) {');
      return false;
    }

    if (!isStringValid(appendix2s.firstTimeTransfer)) {
      console.log('if (!isStringValid(appendix2s.firstTimeTransfer)) {');
      return false;
    }

    if (!isStringValid(appendix2s.timeTransfer)) {
      console.log('if (!isStringValid(appendix2s.timeTransfer)) {');
      return false;
    }

    if (appendix2s.permissionCommision === 'true') {
      if (!isStringValid(appendix2s.permCommissionNumber)) {
        console.log('if (!isStringValid(appendix2s.permCommissionNumber)) {');
        return false;
      }

      if (!isStringValid(appendix2s.permCommissionDate)) {
        console.log('if (!isStringValid(appendix2s.permCommissionDate)) {');
        return false;
      }

      if (!isStringValid(appendix2s.permCommissionDistributor)) {
        console.log('if (!isStringValid(appendix2s.permCommissionDistributor)) {');
        return false;
      }
    }

    if (!this.isNewRules && !appendix2s.insuranceAgreementAbsent) {
      if (!isNotEmptyArray(appendix2s.InsuranceAgreements)) {
        console.log('if (!isNotEmptyArray(appendix2s.InsuranceAgreements)) {');
        return false;
      }
    }

    if (!this.isNewRules && !appendix2s.bankGuarantorAbsent) {
      if (!isStringValid(appendix2s.bankGuarantorName)) {
        console.log('if (!isStringValid(appendix2s.bankGuarantorName)) {');
        return false;
      }

      if (!isStringValid(appendix2s.bankGuarantorINN)) {
        console.log('if (!isStringValid(appendix2s.bankGuarantorINN)) {');
        return false;
      }

      if (!isINNValid(appendix2s.bankGuarantorINN)) {
        this.showInnError = true;
        return false;
      }

      if (!isStringValid(appendix2s.bankGuarantorPlace)) {
        console.log('if (!isStringValid(appendix2s.bankGuarantorPlace)) {');
        return false;
      }

      if (!isStringValid(appendix2s.contractGuaranteeDate)) {
        console.log('if (!isStringValid(appendix2s.contractGuaranteeDate)) {');
        return false;
      }

      if (!isStringValid(appendix2s.contractGuaranteeNumber)) {
        console.log('if (!isStringValid(appendix2s.contractGuaranteeNumber)) {');
        return false;
      }

      if (!isStringValid(appendix2s.contractGuaranteeEndDate)) {
        console.log('if (!isStringValid(appendix2s.contractGuaranteeEndDate)) {');
        return false;
      }
    }

    if (!this.isNewRules) {
      if (!isStringValid(appendix2s.compensationFundAmount)) {
        console.log('if (!isStringValid(appendix2s.compensationFundAmount)) {');
        return false;
      }

      if (!isStringValid(appendix2s.citizensRightsFundAmount)) {
        console.log('if (!isStringValid(appendix2s.citizensRightsFundAmount)) {');
        return false;
      }
    }

    if (appendix2s.ScheduleWorks.some((scheduleWork) => (this.isScheduleWorkHasErrors(scheduleWork)))) {
      return false;
    }

    return true;
  }

  isScheduleWorkHasErrors(scheduleWork) {
    const scheduleWorkItems: ScheduleWorkItem[] = scheduleWork.ScheduleWorkItemss;

    if (scheduleWorkItems.length === 0) {
      return true;
    }

    if (scheduleWork.workType.value === 70012) {
      const hasNotFilledDate = scheduleWorkItems.some((scheduleWorkItem: ScheduleWorkItem) => {
        return !scheduleWorkItem.permission_plandate;
      });

      if (hasNotFilledDate) {
        return true;
      }

      return false;
    }

    if (scheduleWork.workType.value === 70011) {
      const hasNotFilledDate = scheduleWorkItems.some((scheduleWorkItem: ScheduleWorkItem) => {
        return !scheduleWorkItem.conformity_plandate;
      });

      if (hasNotFilledDate) {
        return true;
      }

      return false;
    }

    if (scheduleWork.workType.value === 70013) {
      const hasNotFilledDate = scheduleWorkItems.some((scheduleWorkItem: ScheduleWorkItem) => {
        return !scheduleWorkItem.transfer_plandate;
      });

      if (hasNotFilledDate) {
        return true;
      }

      return false;
    }

    const hasCurrentPeriodOrAlreadyFinished = scheduleWorkItems.some((scheduleWorkItem: ScheduleWorkItem) => {
      if (+scheduleWorkItem.plany < +this.year) {
        return +scheduleWorkItem.factpc === 100;
      } else if (+scheduleWorkItem.plany === +this.year) {
        if (+scheduleWorkItem.planq < +this.ikvartal) {
          return +scheduleWorkItem.factpc === 100;
        } else if (+scheduleWorkItem.planq === +this.ikvartal) {
          return true;
        }
      }
    });

    return !hasCurrentPeriodOrAlreadyFinished;
  }

  public showErrorModal() {
    this.modalService.show(GeneralInformationErrorComponent, { class: 'modal-lg', initialState: { showAppendix2Error: true } });
  }

  save() {
    if (!this.isValid()) {
      this.showErrors = true;
      this.showErrorModal();
      return;
    }

    this.activeModal.hide();
    const cadastralNumbers = this.appendix2.cadastralNumber.slice(0, -1);
    this.appendix2.cadastralNumber = cadastralNumbers;
    this.event.emit(this.appendix2);
  }
}
