import { Component, Input, EventEmitter, OnInit } from '@angular/core';
import { ScheduleWork } from 'src/app/models/report.model';
import { BsModalRef } from 'ngx-bootstrap';
import * as _ from 'lodash';
import { isStringValid } from '../utils/validation.utils';

@Component({
  selector: 'app-schedule-work-modal',
  templateUrl: './schedule-work.component.html',
})
export class ScheduleWorkComponent implements OnInit {
  @Input() scheduleWork: ScheduleWork;
  @Input() ikvartal: string;
  @Input() year: string;

  public event: EventEmitter<any> = new EventEmitter();

  public years: string[] = ['2019', '2020', '2021', '2022', '2023', '2024', '2025'];
  public ikvartals: any[] = [
    {
      name: '1 квартал',
      value: '1',
    },
    {
      name: '2 квартал',
      value: '2',
    },
    {
      name: '3 квартал',
      value: '3',
    },
    {
      name: '4 квартал',
      value: '4',
    },
  ];

  permission_plandate: any;
  permission_factdate: any;
  conformity_plandate: any;
  conformity_factdate: any;
  transfer_plandate: any;
  transfer_finish: any;
  transfer_factdate: any;

  hasFullFactPc = false;

  mode: any;

  showErrors = false;

  showPeriodsError = false;

  constructor(public activeModal: BsModalRef) { }

  ngOnInit() {
    switch (this.scheduleWork.workType.value) {
      case 70013:
        this.mode = 'finishing';
        if (this.scheduleWork.ScheduleWorkItemss && this.scheduleWork.ScheduleWorkItemss.length) {
          this.transfer_plandate = this.scheduleWork.ScheduleWorkItemss[0].transfer_plandate;
          this.transfer_finish = this.scheduleWork.ScheduleWorkItemss[0].transfer_finish;
          this.transfer_factdate = this.scheduleWork.ScheduleWorkItemss[0].transfer_factdate;
          this.scheduleWork.ScheduleWorkItemss = [];
        }
        break;
      case 70012:
        this.mode = 'permission_date';
        if (this.scheduleWork.ScheduleWorkItemss && this.scheduleWork.ScheduleWorkItemss.length) {
          this.permission_plandate = this.scheduleWork.ScheduleWorkItemss[0].permission_plandate;
          this.permission_factdate = this.scheduleWork.ScheduleWorkItemss[0].permission_factdate;
          this.scheduleWork.ScheduleWorkItemss = [];
        }
        break;
      case 70011:
        this.mode = 'conformity_date';
        if (this.scheduleWork.ScheduleWorkItemss && this.scheduleWork.ScheduleWorkItemss.length) {
          this.conformity_plandate = this.scheduleWork.ScheduleWorkItemss[0].conformity_plandate;
          this.conformity_factdate = this.scheduleWork.ScheduleWorkItemss[0].conformity_factdate;
          this.scheduleWork.ScheduleWorkItemss = [];
        }
        break;
      default:
        this.mode = 'table';

        this.hasFullFactPc = this.scheduleWork.ScheduleWorkItemss.some((scheduleWorkItem) => {
          return +scheduleWorkItem.factpc === 100;
        });
        break;
    }
  }

  trackScheduleWorkItem(idx) {
    return idx;
  }

  removeScheduleWorkItem(index) {
    this.scheduleWork.ScheduleWorkItemss.splice(index, 1);

    this.hasFullFactPc = this.scheduleWork.ScheduleWorkItemss.some((scheduleWorkItem) => {
      return +scheduleWorkItem.factpc === 100;
    });
  }

  addScheduleWorkItem() {
    switch (this.mode) {
      case 'table':
        const newItem = {
          planq: '',
          plany: '',
          planpc: '',
          factpc: '',
        };
        this.scheduleWork.ScheduleWorkItemss.push(newItem);
        break;
    }
  }

  isValidTableItem(newItem) {
    if (!newItem.planq) {
      return false;
    }
    if (!newItem.plany) {
      return false;
    }
    if (!isStringValid(newItem.planpc)) {
      return false;
    }
    if (+newItem.planpc > 100) {
      return false;
    }
    if (!isStringValid(newItem.factpc)) {
      return false;
    }
    if (+newItem.factpc > 100) {
      return false;
    }
    return true;
  }

  isValidFinishingItem(newItem) {
    if (!newItem.transfer_plandate) {
      return false;
    }
    return true;
  }

  add() {
    switch (this.mode) {
      case 'table': {
        let hasFilledPlanPC = false;
        const fillMap = {};
        let minYear;
        let maxYear;

        this.scheduleWork.ScheduleWorkItemss.forEach((scheduleWorkItem) => {
          if (!fillMap[scheduleWorkItem.plany]) {
            fillMap[scheduleWorkItem.plany] = [];
          }

          fillMap[scheduleWorkItem.plany].push(+scheduleWorkItem.planq);

          if (!minYear || minYear > +scheduleWorkItem.plany) {
            minYear = +scheduleWorkItem.plany;
          }

          if (!maxYear || maxYear < +scheduleWorkItem.plany) {
            maxYear = +scheduleWorkItem.plany;
          }

          hasFilledPlanPC = hasFilledPlanPC || +scheduleWorkItem.planpc === 100;
        });

        console.log('fillMap', fillMap, minYear, maxYear);

        const expectedYears = _.range(minYear, maxYear + 1);
        const missedYears = _.difference(expectedYears, _.keys(fillMap).map((y) => (+y)));

        if (missedYears.length) {
          this.showPeriodsError = true;
        }

        _.each(fillMap, (yearQuarters: number[], year) => {
          const minQuarter = _.min(yearQuarters);

          console.log('minQuarter', minQuarter);

          if (minQuarter === 4) {
            return;
          }

          for (let q: any = minQuarter; q <= 4; q++) {
            if (!yearQuarters.includes(q)) {
              console.log('missed q', q, year);
              this.showPeriodsError = true;
              return true;
            }
          }
        });
        console.log('missedYears', missedYears);
        console.log('showPeriodsError', this.showPeriodsError);

        const hasEmptyFields = this.scheduleWork.ScheduleWorkItemss.some((scheduleWorkItem) => {
          return !isStringValid(scheduleWorkItem.factpc) || !isStringValid(scheduleWorkItem.planpc) || !scheduleWorkItem.planq || !scheduleWorkItem.plany;
        });

        if (hasEmptyFields || !hasFilledPlanPC || this.showPeriodsError) {
          this.showErrors = true;
          return;
        }

        this.hasFullFactPc = this.scheduleWork.ScheduleWorkItemss.some((scheduleWorkItem) => {
          return +scheduleWorkItem.factpc === 100;
        });

        break;
      }

      case 'permission_date': {
        const newItem = {
          permission_plandate: this.permission_plandate,
          permission_factdate: this.permission_factdate,
        };
        if (!this.permission_plandate) {
          this.showErrors = true;
          return;
        }
        this.scheduleWork.ScheduleWorkItemss.push(newItem);

        this.permission_plandate = null;
        this.permission_factdate = null;
        break;
      }

      case 'conformity_date': {
        const newItem = {
          conformity_plandate: this.conformity_plandate,
          conformity_factdate: this.conformity_factdate,
        };
        if (!this.conformity_plandate) {
          this.showErrors = true;
          return;
        }
        this.scheduleWork.ScheduleWorkItemss.push(newItem);

        this.conformity_plandate = null;
        this.conformity_factdate = null;
        break;
      }

      case 'finishing': {
        const newItem = {
          transfer_plandate: this.transfer_plandate,
          transfer_finish: this.transfer_finish,
          transfer_factdate: this.transfer_factdate,
        };
        if (!this.transfer_plandate) {
          this.showErrors = true;
          return;
        }
        this.scheduleWork.ScheduleWorkItemss.push(newItem);

        this.transfer_plandate = null;
        this.transfer_finish = null;
        this.transfer_factdate = null;
        break;
      }
    }

    this.activeModal.hide();
    this.event.emit(this.scheduleWork);
  }
}
