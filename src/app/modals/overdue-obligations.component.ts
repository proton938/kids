import { Component, Input, EventEmitter } from '@angular/core';
import {  ObligationsRefItem } from 'src/app/models/report.model';
import { BsModalRef } from 'ngx-bootstrap';
import { isStringValid } from 'src/app/utils/validation.utils';

@Component({
  selector: 'app-overdue-obligations-modal',
  templateUrl: './overdue-obligations.component.html',
})
export class OverdueObligationsComponent {
  @Input() obligationsRef: ObligationsRefItem;

  showErrors = false;
  public event: EventEmitter<any> = new EventEmitter();

  constructor(public activeModal: BsModalRef) { }

  isValid() {
    if (!isStringValid(this.obligationsRef.oAddress)) {
      return false;
    }

    if (!this.obligationsRef.tDate) {
      return false;
    }

    if (!isStringValid(this.obligationsRef.reason)) {
      return false;
    }

    return true;
  }

  add() {
    if (!this.isValid()) {
      this.showErrors = true;
      return;
    }

    this.activeModal.hide();
    this.event.emit(this.obligationsRef);
  }
}
