import { Component, Input } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap';

@Component({
  selector: 'app-general-information-error-modal',
  templateUrl: './general-information-error.component.html',
})
export class GeneralInformationErrorComponent {
  @Input() showGeneralInformationError = false;
  @Input() showAppendix2Error = false;
  
  constructor(public activeModal: BsModalRef) {}
}
