import { Component, Input, EventEmitter } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';

import { CreditOrganization } from '../models/report.model';
import { isStringValid, isINNValid } from '../utils/validation.utils';

import { GeneralInformationErrorComponent } from './general-information-error.component';

@Component({
  selector: 'app-credit-organization-modal',
  templateUrl: './credit-organization.component.html',
  styleUrls: ['../app.component.scss'],
})
export class CreditOrganizationComponent {
  @Input() creditOrganization: CreditOrganization;
  public event: EventEmitter<any> = new EventEmitter();
  showErrors = false;
  redForm: boolean = false;

  constructor(public activeModal: BsModalRef, private modalService: BsModalService) { }

  isValid() {
    if (!isStringValid(this.creditOrganization.amountBorrowing)) {
      return false;
    }
    if (this.creditOrganization.creditOrganizationINN) {
      if (!isINNValid(this.creditOrganization.creditOrganizationINN)) {
        this.redForm = true;
        return false;
      } else {
        this.redForm = false;
      }
    }

    return true;
  }

  public showErrorModal() {
    this.modalService.show(GeneralInformationErrorComponent, { class: 'modal-lg', initialState: { showAppendix2Error: true } });
  }

  validateLongFloat(event) {
    if (event.target.value !== '') {
      event.target.value = parseFloat(event.target.value).toFixed(2);
    }
  }

  add() {
    if (!this.isValid()) {
      this.showErrors = true;
      // this.showErrorModal();
      return;
    }

    this.activeModal.hide();
    this.event.emit(this.creditOrganization);
  }
}
