import { Component, Input, EventEmitter } from '@angular/core';
import { BuildingPermit } from 'src/app/models/report.model';
import { BsModalRef } from 'ngx-bootstrap';
import { isStringValid } from 'src/app/utils/validation.utils';

@Component({
  selector: 'app-building-permits-modal',
  templateUrl: './building-permits.component.html',
})
export class BuildingPermitsComponent {
  @Input() buildingPermit: BuildingPermit;
  @Input() RNSList: string[];

  showErrors = false;
  public event: EventEmitter<any> = new EventEmitter();

  constructor(public activeModal: BsModalRef) { }

  isValid() {
    if (!isStringValid(this.buildingPermit.permitNumber)) {
      return false;
    }

    if (!this.buildingPermit.permitDate) {
      return false;
    }

    if (!isStringValid(this.buildingPermit.permitDistributor)) {
      return false;
    }

    if (!this.buildingPermit.periodOfValidity) {
      return false;
    }

    return true;
  }

  add() {
    if (!this.isValid()) {
      this.showErrors = true;
      return;
    }

    this.activeModal.hide();
    this.event.emit(this.buildingPermit);
  }
}
