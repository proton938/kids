import { Component, Input, EventEmitter } from '@angular/core';
import { OtherCostsRefItem } from 'src/app/models/report.model';
import { BsModalRef } from 'ngx-bootstrap';
import { isStringValid, isLongFloatValid } from 'src/app/utils/validation.utils';

@Component({
  selector: 'app-other-costs-modal',
  templateUrl: './other-costs.component.html',
})
export class OtherCostsComponent {
  @Input() otherCostsItem: OtherCostsRefItem;

  showErrors = false;
  public event: EventEmitter<any> = new EventEmitter();

  constructor(public activeModal: BsModalRef) { }

  isValid() {
    if (!isStringValid(this.otherCostsItem.name)) {
      return false;
    }

    console.log('before', this.otherCostsItem.sum);
    this.otherCostsItem.sum = parseFloat(this.otherCostsItem.sum).toFixed(2);
    console.log('after', this.otherCostsItem.sum);
    if (!isLongFloatValid(this.otherCostsItem.sum)) {
      return false;
    }

    if (!isStringValid(this.otherCostsItem.pdLink)) {
      return false;
    }

    return true;
  }

  validateLongFloat(event) {
    if (event.target.value !== '') {
      event.target.value = parseFloat(event.target.value).toFixed(2);
    }
  }

  add() {
    if (!this.isValid()) {
      this.showErrors = true;
      return;
    }

    this.activeModal.hide();
    this.event.emit(this.otherCostsItem);
  }
}
