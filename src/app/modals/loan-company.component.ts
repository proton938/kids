import { Component, Input, EventEmitter } from '@angular/core';
import { LoanCompany } from 'src/app/models/report.model';
import { BsModalRef } from 'ngx-bootstrap';
import { isINNValid, isStringValid } from 'src/app/utils/validation.utils';

@Component({
  selector: 'app-loan-company-modal',
  templateUrl: './loan-company.component.html',
})
export class LoanCompanyComponent {
  @Input() loanCompany: LoanCompany;
  public event: EventEmitter<any> = new EventEmitter();

  showErrors = false;

  constructor(public activeModal: BsModalRef) { }

  validateLongFloat(event) {
    if (event.target.value !== '') {
      event.target.value = parseFloat(event.target.value).toFixed(2)
    }
  }

  isValid() {
    if (isStringValid(this.loanCompany.loanCompanyINN) && !isINNValid(this.loanCompany.loanCompanyINN)) {
      return false;
    }

    return true;
  }

  add() {
    if (!this.isValid()) {
      this.showErrors = true;
      return false;
    }

    this.activeModal.hide();
    this.event.emit(this.loanCompany);
  }
}
