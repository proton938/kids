import * as _ from 'lodash';

export function convertBoolIfNeeded(obj, field) {
  const val = _.get(obj, field, false);

  if (val === false) {
    return 'false';
  }

  return 'true';
}

export function convertToArrayIfNeeded(obj, field) {
  const val = _.get(obj, field, []);

  if (_.isArray(val)) {
    return val;
  }

  if (_.isObject(val)) {
    return _.values(val);
  }

  return val;
}
