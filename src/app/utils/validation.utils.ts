export function isStringValid(str) {
  if (str === undefined) {
    return false;
  }

  if (str.trim && str.trim() === '') {
    return false;
  }

  return true;
}


export function isLongFloatValid(str) {
  return /\d{1,}.\d{2,}/.test(str);
}


export function isNotEmptyArray(arr) {
  if (!arr) {
    return false;
  }

  if (arr.length === 0) {
    return false;
  }

  return true;
}

export function isINNValid(innStr) {
  if (!isStringValid(innStr)) {
    return false;
  }
  let result = false;
  const checkDigit = (inn, coefficients): number => {
    let n = 0;
    for (let i in coefficients) {
      n += coefficients[i] * inn[i];
    }

    return parseInt((n % 11 % 10).toString(), 10);
  };

  switch (innStr.length) {
    case 10:
      const n10 = checkDigit(innStr, [2, 4, 10, 3, 5, 9, 4, 6, 8]);

      if (n10 === parseInt(innStr[9], 10)) {
        result = true;
      }
      break;
    case 12:
      const n11 = checkDigit(innStr, [7, 2, 4, 10, 3, 5, 9, 4, 6, 8]);
      const n12 = checkDigit(innStr, [3, 7, 2, 4, 10, 3, 5, 9, 4, 6, 8]);

      if ((n11 === parseInt(innStr[10], 10)) && (n12 === parseInt(innStr[11], 10))) {
        result = true;
      }
      break;
  }

  return result;
}