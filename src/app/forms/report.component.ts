import { Component, Input, ViewEncapsulation, ViewChild } from '@angular/core';
import { NgWizardConfig, THEME, StepChangedArgs, NgWizardService } from 'ng-wizard';
import { v4 as uuidv4 } from 'uuid';
import { saveAs } from 'file-saver';
import * as _ from 'lodash';

import { Report } from '../models/report.model';

import 'ng-wizard/themes/ng_wizard.min.css';
import './report.component.css';
import { GeneralInformationComponent } from '../steps/general-information.component';
import { ErrorComponent } from '../modals/error.component';
import { BsModalService } from 'ngx-bootstrap';
import { RatesComponent } from '../steps/rates.component';
import { ReferenceComponent } from '../steps/reference.component';
import { HttpClient } from '@angular/common/http';
import { StatementComponent } from '../steps/statement.component';
import { convertBoolIfNeeded, convertToArrayIfNeeded } from '../utils/conversion.utils';
import { formatDate, DatePipe } from "@angular/common";

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.css'],
})
export class ReportComponent {

  encapsulation: ViewEncapsulation.None;

  config: NgWizardConfig = {
    selected: 0,
    theme: THEME.default,
    lang: {
      previous: 'Назад',
      next: 'Далее',
    },
    toolbarSettings: {
      showNextButton: false,
      showPreviousButton: false,
      toolbarExtraButtons: [
        {
          text: 'Назад',
          class: 'btn btn-default',
          event: () => {
            this.showPreviousStep();
          },
        },
        {
          text: 'Далее',
          class: 'btn btn-primary mr-3 ml-2',
          event: () => {
            this.showNextStep();
          },
        },
      ],
    },
  };

  stepValidationStatus = {
    0: false,
    1: false,
    2: false,
  };

  currentStepIndex = 0;

  loaded = true;

  @Input() report: Report = new Report();

  @ViewChild(GeneralInformationComponent, null) generalInformation: GeneralInformationComponent;
  @ViewChild(RatesComponent, null) rates: RatesComponent;
  @ViewChild(ReferenceComponent, null) reference: ReferenceComponent;
  @ViewChild(StatementComponent, null) statement: StatementComponent;
  @ViewChild('loadedReport', null) loadedReport;
  @ViewChild('loadedXMLReport', null) loadedXMLReport;

  constructor(private ngWizardService: NgWizardService, private modalService: BsModalService, private httpService: HttpClient) {
  }

  exportJSON() {
    const exportData = JSON.stringify(this.report);
    const blob = new Blob([exportData], { type: 'text/json;charset=utf-8' });

    saveAs(blob, `${uuidv4()}.dat`);
  }

  importJSON = () => {
    this.loadedReport.nativeElement.click();
  }

  importXML = () => {
    this.loadedXMLReport.nativeElement.click();
  }

  handleXMLFileInput(files) {
    const importFile = files.item(0);
    const formData: FormData = new FormData();
    formData.append('xml', importFile);

    this.httpService.post('https://smart-dev.reinform-int.ru/dol/api/import_xml/', formData, { responseType: 'text' }).subscribe(
      (JSONText) => {
        const oldReport = JSON.parse(JSONText);
        const boolFieldsToConvert = [
          'seaIsOrganization',
          ''
        ];
        const arrayFieldsToConvert = [

        ];

        _.forEach(boolFieldsToConvert, (key) => {
          _.set(oldReport, key, convertBoolIfNeeded(oldReport, key));
        });

        _.forEach(arrayFieldsToConvert, (key) => {
          _.set(oldReport, key, convertToArrayIfNeeded(oldReport, key));
        });

        _.forEach(oldReport.Appendix2s, (appendix2) => {
          if (!appendix2.ScheduleWorks) {
            appendix2.ScheduleWorks = [
              { workType: { value: 70001, name: 'Подготовительные, геодезические работы' }, ScheduleWorkItemss: [] },
              { workType: { value: 70002, name: 'Земляные работы' }, ScheduleWorkItemss: [] },
              { workType: { value: 70003, name: 'Работы при возведении конструкций фундаментов' }, ScheduleWorkItemss: [] },
              { workType: { value: 70004, name: 'Работы при возведении конструкций подземной части' }, ScheduleWorkItemss: [] },
              { workType: { value: 70005, name: 'Работы при возведении конструкций надземной части' }, ScheduleWorkItemss: [] },
              { workType: { value: 70006, name: 'Работы при устройстве внутридомовых сетей инженерно-технического обеспечения' }, ScheduleWorkItemss: [] },
              { workType: { value: 70007, name: 'Работы при устройстве внутриплощадочных сетей инженерно-технического обеспечения' }, ScheduleWorkItemss: [] },
              { workType: { value: 70008, name: 'Работы по благоустройству объекта недвижимости' }, ScheduleWorkItemss: [] },
              { workType: { value: 70010, name: 'Степень готовности объекта недвижимости (%)' }, ScheduleWorkItemss: [] },
              { workType: { value: 70012, name: 'Получение заключения о соответствии (дата)' }, ScheduleWorkItemss: [] },
              { workType: { value: 70011, name: 'Получение разрешения на ввод объекта в эксплуатацию (дата)' }, ScheduleWorkItemss: [] },
              { workType: { value: 70013, name: 'Срок передачи объектов долевого строительства участникам долевого строительства' }, ScheduleWorkItemss: [] },
            ];
          }

          _.forEach(appendix2.ScheduleWorks, (scheduleWork) => {
            if (!scheduleWork.workType) {
              scheduleWork.workType = {
                value: Number(scheduleWork.code),
                name: scheduleWork.name,
              };
            }
            if (!scheduleWork.ScheduleWorkItemss) {
              scheduleWork.ScheduleWorkItemss = [
                ...scheduleWork.items,
              ];
            }
          });

          const boolFieldsToConvert = [
            'infBorrowing',
            'permissionCommision',
            'partiallyEntered',
            // 'insuranceAgreementAbsent'
          ];

          _.forEach(boolFieldsToConvert, (key) => {
            _.set(appendix2, key, convertBoolIfNeeded(appendix2, key));
          });
        });

        this.report = oldReport;
      },
      (error) => {
        this.modalService.show(ErrorComponent, {
          initialState: {
            title: 'Ошибка при загрузке файла',
            message: (error && error.message) ? error.message : '',
          }
        });
        console.log('error', error);
      }
    );
  }

  handleFileInput(files) {
    const importFile = files.item(0);
    const reader = new FileReader();

    reader.onload = () => {
      try {
        const data = JSON.parse(reader.result as string);
        data.regDate = new Date(data.regDate);
        // this.report.regDate = new Date();
        setTimeout(() => {
          try {
            this.report = data;
            this.loaded = true;
          } catch (e) {
            console.log('Import2222 error', e);
          }
        });
      } catch (error) {
        this.modalService.show(ErrorComponent, {
          initialState: {
            title: 'Ошибка при загрузке файла',
            message: (error && error.message) ? error.message : '',
          }
        });
      }
    };

    this.loaded = false;
    reader.readAsText(importFile);
  }

  showPreviousStep(event?: Event) {
    this.ngWizardService.previous();
  }

  showNextStep(event?: Event) {
    switch (this.currentStepIndex) {
      case 0: {
        if (!this.generalInformation.isValid()) {
          this.generalInformation.showErrorModal();
          return;
        }

        break;
      }
      case 1: {
        if (
          !this.report.balanceNodes.assets ||
          !this.report.balanceNodes.liabilities ||
          !this.report.balanceNodes.report
        ) {
          this.modalService.show(ErrorComponent, {
            initialState: {
              title: 'Ошибка',
              message: 'Добавьте файлы экспорта бухгалтерского баланса за выбранный период',
            }
          });
          return;
        }
        break;
      }

      case 2: {
        if (!this.rates.isValid()) {
          this.rates.showErrors = true;
          return;
        }

        break;
      }

      case 3: {
        if (!this.reference.isValid()) {
          this.reference.showError();
          return;
        }

        break;
      }

      case 4: {
        if (!this.statement.isValid()) {
          this.statement.showError();
          return;
        }

        break;
      }
    }

    this.ngWizardService.next();
  }

  canShowAdditionalStep() {
    const hasNoPermissionOrPartiallyEntered = this.report.Appendix2s
      .some((appendix) => (appendix.permissionCommision !== 'true' || appendix.partiallyEntered !== 'false'));

    if (!hasNoPermissionOrPartiallyEntered) {
      return false;
    }

    return true;
  }

  stepChanged(args: StepChangedArgs) {
    const previousStepIndex = this.currentStepIndex;
    this.currentStepIndex = args.step.index;

    if (!this.canShowAdditionalStep() && (this.currentStepIndex === 2 || this.currentStepIndex === 4)) {
      if (previousStepIndex > this.currentStepIndex) {
        setTimeout(() => {
          this.ngWizardService.previous();
        });
      } else {
        setTimeout(() => {
          this.ngWizardService.next();
        });
      }
    }
  }
}
