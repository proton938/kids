import { AppComponent } from './app.component';

export const statesConfig = [
  {
    name: 'app',
    url: '*',
    component: AppComponent
  },
];