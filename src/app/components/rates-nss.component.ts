import { Component, Input, OnInit } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap';
import * as _ from 'lodash';

import { Report } from '../models/report.model';
import { ErrorComponent } from 'src/app/modals/error.component';

@Component({
  selector: 'app-rates-nss',
  templateUrl: './rates-nss.component.html',
  styleUrls: ['./rates-nss.component.scss']
})
export class RatesNSSComponent implements OnInit {

  constructor(private modalService: BsModalService) {
  }
  @Input() report: Report = new Report();

  loading = false;
  showErrors = false;

  sumReportCost = 0;

  b1400Row: any;
  b1500Row: any;
  b1420Row: any;

  b1400: any;
  b1500: any;
  b1420: any;

  unfinishedBuildingActivities673 = 0;
  taxesAndFees673 = 0;
  finishedBuildingActivities673 = 0;
  prepayment673 = 0;
  otherCosts673 = 0;
  contractDebts673 = 0;
  valueAddedTax673 = 0;
  cashInBankAccounts673 = 0;
  revenueOfFuturePeriods673 = 0;

  unfinishedBuildingActivities673Rows = [];
  taxesAndFees673Rows = [];
  finishedBuildingActivities673Rows = [];
  prepayment673Rows = [];
  otherCosts673Rows = [];
  contractDebts673Rows = [];
  valueAddedTax673Rows = [];
  cashInBankAccounts673Rows = [];
  revenueOfFuturePeriods673Rows = [];

  ngOnInit() {
    this.calcNSS();
  }

  getActualValueByCode(code: string) {
    if (+code.slice(0, 2) > 12 && code !== '1600') {
      return this.getActualValue(this.getLiabilityRowByCode(code));
    } else {
      return this.getActualValue(this.getAssetRowByCode(code));
    }
  }

  getActualValue = (row) => {
    return row.values.find((value) => (value.quarter === +this.report.ikvartal && value.year === +this.report.year)).value;
  }

  getRowByCode(code) {
    if (+code.slice(0, 2) > 12 && code !== '1600') {
      return this.getLiabilityRowByCode(code);
    } else {
      return this.getAssetRowByCode(code);
    }
  }

  getRowsByField(field, value) {
    return [
      ...this.report.balanceNodes.assets.filter((node) => (node[field] === value)),
      ...this.report.balanceNodes.liabilities.filter((node) => (node[field] === value)),
    ];
  }

  getAssetRowByCode(code) {
    return this.report.balanceNodes.assets.find((node) => (node.code === code));
  }

  getLiabilityRowByCode(code) {
    return this.report.balanceNodes.liabilities.find((node) => (node.code === code));
  }

  calcNSS() {
    this.sumReportCost = 0;
    this.unfinishedBuildingActivities673 = 0;
    this.taxesAndFees673 = 0;
    this.finishedBuildingActivities673 = 0;
    this.prepayment673 = 0;
    this.otherCosts673 = 0;
    this.contractDebts673 = 0;
    this.valueAddedTax673 = 0;
    this.cashInBankAccounts673 = 0;
    this.revenueOfFuturePeriods673 = 0;

    this.unfinishedBuildingActivities673Rows = [];
    this.taxesAndFees673Rows = [];
    this.finishedBuildingActivities673Rows = [];
    this.prepayment673Rows = [];
    this.otherCosts673Rows = [];
    this.contractDebts673Rows = [];
    this.valueAddedTax673Rows = [];
    this.cashInBankAccounts673Rows = [];
    this.revenueOfFuturePeriods673Rows = [];

    this.report.Appendix2s.forEach((a) => {
      this.sumReportCost += +a.reportCost;
    });
    this.b1400 = this.getActualValueByCode('1400');
    this.b1400Row = this.getRowByCode('1400');

    this.b1500 = this.getActualValueByCode('1500');
    this.b1500Row = this.getRowByCode('1500');

    this.b1420 = this.getActualValueByCode('1420');
    this.b1420Row = this.getRowByCode('1420');

    if (this.report.withoutUnfinishedBuildingActivities673 !== true) {
      this.unfinishedBuildingActivities673Rows = this.getRowsByField('unfinishedBuildingActivities673', true);
      this.unfinishedBuildingActivities673 = _.sumBy(this.unfinishedBuildingActivities673Rows, this.getActualValue);
    }

    if (this.report.withoutTaxesAndFees673 !== true) {
      this.taxesAndFees673Rows = this.getRowsByField('taxesAndFees673', true);
      this.taxesAndFees673 = _.sumBy(this.taxesAndFees673Rows, this.getActualValue);
    }

    if (this.report.withoutFinishedBuildingActivities673 !== true) {
      this.finishedBuildingActivities673Rows = this.getRowsByField('finishedBuildingActivities673', true);
      this.finishedBuildingActivities673 = _.sumBy(this.finishedBuildingActivities673Rows, this.getActualValue);
    }

    if (this.report.withoutPrepayment673 !== true) {
      this.prepayment673Rows = this.getRowsByField('prepayment673', true);
      this.prepayment673 = _.sumBy(this.prepayment673Rows, this.getActualValue);
    }

    if (this.report.withoutOtherCosts673 !== true) {
      this.otherCosts673Rows = this.getRowsByField('otherCosts673', true);
      this.otherCosts673 = _.sumBy(this.otherCosts673Rows, this.getActualValue);
    }
    if (this.report.withoutContractDebts673 !== true) {
      this.contractDebts673Rows = this.getRowsByField('contractDebts673', true);
      this.contractDebts673 = _.sumBy(this.contractDebts673Rows, this.getActualValue);
    }

    if (this.report.withoutValueAddedTax673 !== true) {
      this.valueAddedTax673Rows = this.getRowsByField('valueAddedTax673', true);
      this.valueAddedTax673 = _.sumBy(this.valueAddedTax673Rows, this.getActualValue);
    }

    if (this.report.withoutCashInBankAccounts673 !== true) {
      this.cashInBankAccounts673Rows = this.getRowsByField('cashInBankAccounts673', true);
      this.cashInBankAccounts673 = _.sumBy(this.cashInBankAccounts673Rows, this.getActualValue);
    }

    if (this.report.withoutRevenueOfFuturePeriods673 !== true) {
      this.revenueOfFuturePeriods673Rows = this.getRowsByField('revenueOfFuturePeriods673', true);
      this.revenueOfFuturePeriods673 = _.sumBy(this.revenueOfFuturePeriods673Rows, this.getActualValue);
    }

    this.report.pf = this.unfinishedBuildingActivities673 + this.taxesAndFees673 + this.finishedBuildingActivities673 + this.prepayment673 +
      this.otherCosts673 + this.contractDebts673 + this.valueAddedTax673 +
      this.cashInBankAccounts673 - (this.b1400 + this.b1500 - this.revenueOfFuturePeriods673 - this.b1420);

    const k = this.report.measuringUnit === 'RUBLES' ? 1000000 : 1000;
    const b = this.sumReportCost * k;

    this.report.normPf = b ? ((this.report.pf / b) * 100).toFixed(2) : '';
  }
}
