import { Component, Input, OnInit } from '@angular/core';
import * as _ from 'lodash';

@Component({
  selector: 'app-balance-table',
  templateUrl: './balance-table.component.html',
})
export class BalanceTableComponent implements OnInit {
  @Input() tableData: any;
  @Input() tableCode: any;
  @Input() isReport = false;

  quarterLastDayMap = {
    1: '31 марта',
    2: '30 июня',
    3: '30 сентября',
    4: '31 декабря',
  };

  quarterLastMonthMap = {
    1: 'Март',
    2: 'Июнь',
    3: 'Сентябрь',
    4: 'Декабрь',
  };

  tablesByCode: any = [];

  valuesColumnNames: any = [];
  numberOfColumns = 0;


  isBoldRow(code) {
    return (code = parseFloat(code)) < 10000 && code >= 1000 && code % 100 === 0;
  }

  ngOnInit() {
    this.tablesByCode = this.groupRowsByCode(this.tableData);
    this.valuesColumnNames = this.generateTableValuesColumnNames(this.tableData[0]);
    this.numberOfColumns = this.getNumberOfColumns(this.tableData[0]);
  }

  getNumberOfColumns(row) {
    return 2 + row.values.length;
  }

  generateTableValuesColumnNames(row: any) {
    if (this.isReport) {
      return this.generateReportTableValuesColumnNames(row);
    }

    return row.values.map((value) => {
      return `На ${this.quarterLastDayMap[+value.quarter]} ${value.year} г.`;
    });
  }

  generateReportTableValuesColumnNames(row: any) {
    return row.values.map((value) => {
      return `За Январь - ${this.quarterLastMonthMap[+value.quarter]} ${value.year} г.`;
    });
  }

  groupRowsByCode(rows: any[]) {
    return _.values(_.groupBy(rows, (row) => (row.code.substr(0, 2))));
  }
}
