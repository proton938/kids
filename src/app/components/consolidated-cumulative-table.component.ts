import { Component, Input, OnInit } from '@angular/core';
import * as _ from 'lodash';
import { Report, SheetRow } from '../models/report.model';
import { isStringValid } from '../utils/validation.utils';
import { ReportService } from "../models/report.service";

@Component({
  selector: 'app-consolidated-cumulative-table',
  templateUrl: './consolidated-cumulative-table.component.html',
})
export class ConsolidatedCumulativeTableComponent implements OnInit {
  @Input() tableData: SheetRow[];

  reportConsolidatedCumulativeSheets: any;

  constructor(private reportService: ReportService) {
  }

  showErrorPrevCost = false;
  showErrorTotalCost = false;
  showErrorPaidWorks = false;
  showErrorAssimilatedWorks = false;
  showErrorBalanceForPayment = false;

  addedLines = {
    1001001000: [1001001004, 1001001005, 1001001006, 1001001007, 1001001008, 1001001009, 1001001010, 1001001011, 1001001012, 1001001013],
    1001002000: [1001002016, 1001002017, 1001002018, 1001002019, 1001002020, 1001002021, 1001002022, 1001002023, 1001002024, 1001002025],
    1001003000: [1001003003, 1001003004, 1001003005, 1001003006, 1001003007, 1001003008, 1001003009, 1001003010, 1001003011, 1001003012],
    1002000000: [1002004000, 1002005000, 1002006000, 1002007000, 1002008000, 1002009000, 1002010000, 1002011000, 1002012000, 1002013000],
    1003000000: [1003005000, 1003006000, 1003007000, 1003008000, 1003009000, 1003010000, 1003011000, 1003012000, 1003013000, 1003014000],
    1004000000: [1004008000, 1004009000, 1004010000, 1004011000, 1004012000, 1004013000, 1004014000, 1004015000, 1004016000, 1004017000],
    1005000000: [1005008000, 1005009000, 1005010000, 1005011000, 1005012000, 1005013000, 1005014000, 1005015000, 1005016000, 1005017000],
    1006000000: [1006002000, 1006003000, 1006004000, 1006005000, 1006006000, 1006007000, 1006008000, 1006009000, 1006010000, 1006011000],
    1007000000: [1007002000, 1007003000, 1007004000, 1007005000, 1007006000, 1007007000, 1007008000, 1007009000, 1007010000, 1007011000],
    1008000000: [1008003000, 1008004000, 1008005000, 1008006000, 1008007000, 1008008000, 1008009000, 1008010000, 1008011000, 1008012000],
    1009000000: [1009010000, 1009011000, 1009012000, 1009013000, 1009014000, 1009015000, 1009016000, 1009017000, 1009018000, 1009019000],
    1010000000: [1010004000, 1010005000, 1010006000, 1010007000, 1010008000, 1010009000, 1010010000, 1010011000, 1010012000, 1010013000]
  };

  articleOfCalculatingDictItems = [
    {
      id: 1001000000,
      paragraph: '1',
      articleOfCalculating: '1',
      name: 'Статья 1. Стоимость строительства, в том числе:',

      calc: true
    },
    {
      id: 1001001000,
      paragraph: '1',
      articleOfCalculating: '1.1',
      name: 'Подготовка территории строительства, в том числе:',

      calc: true,
      parent: 1001000000
    },
    {
      id: 1001001001,
      paragraph: '1',
      articleOfCalculating: '1.1.1',
      name: 'снос строений; вырубка деревьев; планировка площадки',

      parent: 1001001000
    },
    {
      id: 1001001002,
      paragraph: '1',
      articleOfCalculating: '1.1.2',
      name: 'вынос сетей из пятна застройки',

      parent: 1001001000
    },
    {
      id: 1001001003,
      paragraph: '1',
      articleOfCalculating: '1.1.3',
      name: 'мобилизация; бытовой городок',

      parent: 1001001000
    },
    {
      id: 1001001004,
      paragraph: '1',
      articleOfCalculating: '1.1.4',
      name: '',

      parent: 1001001000
    },
    {
      id: 1001001005,
      paragraph: '1',
      articleOfCalculating: '1.1.5',
      name: '',

      parent: 1001001000
    },
    {
      id: 1001001006,
      paragraph: '1',
      articleOfCalculating: '1.1.6',
      name: '',

      parent: 1001001000
    },
    {
      id: 1001001007,
      paragraph: '1',
      articleOfCalculating: '1.1.7',
      name: '',

      parent: 1001001000
    },
    {
      id: 1001001008,
      paragraph: '1',
      articleOfCalculating: '1.1.8',
      name: '',

      parent: 1001001000
    },
    {
      id: 1001001009,
      paragraph: '1',
      articleOfCalculating: '1.1.9',
      name: '',

      parent: 1001001000
    },
    {
      id: 1001001010,
      paragraph: '1',
      articleOfCalculating: '1.1.10',
      name: '',

      parent: 1001001000
    },
    {
      id: 1001001011,
      paragraph: '1',
      articleOfCalculating: '1.1.11',
      name: '',

      parent: 1001001000
    },
    {
      id: 1001001012,
      paragraph: '1',
      articleOfCalculating: '1.1.12',
      name: '',

      parent: 1001001000
    },
    {
      id: 1001001013,
      paragraph: '1',
      articleOfCalculating: '1.1.13',
      name: '',

      parent: 1001001000,
      show_total: 'Всего по пункту 1.1'
    }, {
      id: 1001002000,
      paragraph: '1',
      articleOfCalculating: '1.2',
      name: 'Строительно-монтажные работы, в том числе:',

      calc: true,
      parent: 1001000000
    }, {
      id: 1001002001,
      paragraph: '1',
      articleOfCalculating: '1.2.1',
      name: 'устройство котлована (шпунтовое ограждение, земляные работы)',

      parent: 1001002000
    }, {
      id: 1001002002,
      paragraph: '1',
      articleOfCalculating: '1.2.2',
      name: 'обратная засыпка котлована',

      parent: 1001002000
    }, {
      id: 1001002003,
      paragraph: '1',
      articleOfCalculating: '1.2.3',
      name: 'устройство конструкций нулевого цикла (в том числе фундаменты, гидроизоляция)',

      parent: 1001002000
    }, {
      id: 1001002004,
      paragraph: '1',
      articleOfCalculating: '1.2.4',
      name: 'устройство конструкций надземной части',

      parent: 1001002000
    }, {
      id: 1001002005,
      paragraph: '1',
      articleOfCalculating: '1.2.5',
      name: 'устройство наружных ограждающих конструкций (в том числе установка оконных блоков, отделочные работы)',

      parent: 1001002000
    }, {
      id: 1001002006,
      paragraph: '1',
      articleOfCalculating: '1.2.6',
      name: 'устройство кровли',

      parent: 1001002000
    }, {
      id: 1001002007,
      paragraph: '1',
      articleOfCalculating: '1.2.7',
      name: 'устройство внутренних стен и перегородок',

      parent: 1001002000
    }, {
      id: 1001002008,
      paragraph: '1',
      articleOfCalculating: '1.2.8',
      name: 'проведение внутренних отделочных работ (в том числе установки дверных блоков, отделки стен и потолков, устройства полов)',

      parent: 1001002000
    }, {
      id: 1001002009,
      paragraph: '1',
      articleOfCalculating: '1.2.9',
      name: 'монтаж лифтов',

      parent: 1001002000
    }, {
      id: 1001002010,
      paragraph: '1',
      articleOfCalculating: '1.2.10',
      name: 'устройство внутридомовых систем холодного и горячего водоснабжения',

      parent: 1001002000
    }, {
      id: 1001002011,
      paragraph: '1',
      articleOfCalculating: '1.2.11',
      name: 'устройство внутридомовых инженерных систем отопления',

      parent: 1001002000
    }, {
      id: 1001002012,
      paragraph: '1',
      articleOfCalculating: '1.2.12',
      name: 'устройство внутридомовых инженерных систем канализации и ливнестока',

      parent: 1001002000
    }, {
      id: 1001002013,
      paragraph: '1',
      articleOfCalculating: '1.2.13',
      name: 'устройство внутридомовых инженерных систем электроснабжения',

      parent: 1001002000
    }, {
      id: 1001002014,
      paragraph: '1',
      articleOfCalculating: '1.2.14',
      name: 'устройство внутридомовых инженерных систем слаботочных устройств',

      parent: 1001002000
    }, {
      id: 1001002015,
      paragraph: '1',
      articleOfCalculating: '1.2.15',
      name: 'устройство внутридомовых инженерных систем противопожарной автоматики',

      parent: 1001002000
    }, {
      id: 1001002016,
      paragraph: '1',
      articleOfCalculating: '1.2.16',
      name: '',

      parent: 1001002000
    }, {
      id: 1001002017,
      paragraph: '1',
      articleOfCalculating: '1.2.17',
      name: '',

      parent: 1001002000
    }, {
      id: 1001002018,
      paragraph: '1',
      articleOfCalculating: '1.2.18',
      name: '',

      parent: 1001002000
    }, {
      id: 1001002019,
      paragraph: '1',
      articleOfCalculating: '1.2.19',
      name: '',

      parent: 1001002000
    }, {
      id: 1001002020,
      paragraph: '1',
      articleOfCalculating: '1.2.20',
      name: '',

      parent: 1001002000
    }, {
      id: 1001002021,
      paragraph: '1',
      articleOfCalculating: '1.2.21',
      name: '',

      parent: 1001002000
    }, {
      id: 1001002022,
      paragraph: '1',
      articleOfCalculating: '1.2.22',
      name: '',

      parent: 1001002000
    }, {
      id: 1001002023,
      paragraph: '1',
      articleOfCalculating: '1.2.23',
      name: '',

      parent: 1001002000
    }, {
      id: 1001002024,
      paragraph: '1',
      articleOfCalculating: '1.2.24',
      name: '',

      parent: 1001002000
    }, {
      id: 1001002025,
      paragraph: '1',
      articleOfCalculating: '1.2.25',
      name: '',

      parent: 1001002000,
      show_total: 'Всего по пункту 1.2'
    }, {
      id: 1001003000,
      paragraph: '1',
      articleOfCalculating: '1.3',
      name: 'Объекты транспортного хозяйства и благоустройство, в том числе:',

      calc: true,
      parent: 1001000000
    }, {
      id: 1001003001,
      paragraph: '1',
      articleOfCalculating: '1.3.1',
      name: 'благоустройство (озеленение и малые архитектурные формы);',

      parent: 1001003000
    }, {
      id: 1001003002,
      paragraph: '1',
      articleOfCalculating: '1.3.2',
      name: 'дороги и прилегающие территории',

      parent: 1001003000
    }, {
      id: 1001003003,
      paragraph: '1',
      articleOfCalculating: '1.3.3',
      name: '',

      parent: 1001003000
    }, {
      id: 1001003004,
      paragraph: '1',
      articleOfCalculating: '1.3.4',
      name: '',

      parent: 1001003000
    }, {
      id: 1001003005,
      paragraph: '1',
      articleOfCalculating: '1.3.5',
      name: '',

      parent: 1001003000
    }, {
      id: 1001003006,
      paragraph: '1',
      articleOfCalculating: '1.3.6',
      name: '',

      parent: 1001003000
    }, {
      id: 1001003007,
      paragraph: '1',
      articleOfCalculating: '1.3.7',
      name: '',

      parent: 1001003000
    }, {
      id: 1001003008,
      paragraph: '1',
      articleOfCalculating: '1.3.8',
      name: '',

      parent: 1001003000
    }, {
      id: 1001003009,
      paragraph: '1',
      articleOfCalculating: '1.3.9',
      name: '',

      parent: 1001003000
    }, {
      id: 1001003010,
      paragraph: '1',
      articleOfCalculating: '1.3.10',
      name: '',

      parent: 1001003000
    }, {
      id: 1001003011,
      paragraph: '1',
      articleOfCalculating: '1.3.11',
      name: '',

      parent: 1001003000
    }, {
      id: 1001003012,
      paragraph: '1',
      articleOfCalculating: '1.3.12',
      name: '',

      parent: 1001003000,
      show_total: 'Всего по пункту 1.3'
    }, {
      id: 1001004000,
      paragraph: '1',
      articleOfCalculating: '1.4',
      name: 'Иное',

      parent: 1001000000,
      show_total: 'Всего по статье 1'
    }, {
      id: 1002000000,
      paragraph: '2',
      articleOfCalculating: '2',
      name: 'Статья 2. Стоимость прав реализации проекта, в том числе:',
      calc: true,

    }, {
      id: 1002001000,
      paragraph: '2',
      articleOfCalculating: '2.1',
      name: 'затраты на приобретение земельного участка, изменение его разрешенного использования',

      parent: 1002000000
    }, {
      id: 1002001001,
      paragraph: '2',
      articleOfCalculating: '2.1.1',
      name: 'затраты на изменение разрешенного использования земельного участка и снятие запрета',

      parent: 1002000000
    }, {
      id: 1002002000,
      paragraph: '2',
      articleOfCalculating: '2.2',
      name: 'затраты на право застройки и аренду земельного участка',

      parent: 1002000000
    }, {
      id: 1002003000,
      paragraph: '2',
      articleOfCalculating: '2.3',
      name: 'возмещение убытков собственникам земельного участка, бывшим владельцам земельного участка, арендаторам земельного участка',

      parent: 1002000000
    }, {
      id: 1002004000,
      paragraph: '2',
      articleOfCalculating: '2.4',
      name: '',

      parent: 1002000000
    }, {
      id: 1002005000,
      paragraph: '2',
      articleOfCalculating: '2.5',
      name: '',

      parent: 1002000000
    }, {
      id: 1002006000,
      paragraph: '2',
      articleOfCalculating: '2.6',
      name: '',

      parent: 1002000000
    }, {
      id: 1002007000,
      paragraph: '2',
      articleOfCalculating: '2.7',
      name: '',

      parent: 1002000000
    }, {
      id: 1002008000,
      paragraph: '2',
      articleOfCalculating: '2.8',
      name: '',

      parent: 1002000000
    }, {
      id: 1002009000,
      paragraph: '2',
      articleOfCalculating: '2.9',
      name: '',

      parent: 1002000000
    }, {
      id: 1002010000,
      paragraph: '2',
      articleOfCalculating: '2.10',
      name: '',

      parent: 1002000000
    }, {
      id: 1002011000,
      paragraph: '2',
      articleOfCalculating: '2.11',
      name: '',

      parent: 1002000000
    }, {
      id: 1002012000,
      paragraph: '2',
      articleOfCalculating: '2.12',
      name: '',

      parent: 1002000000
    }, {
      id: 1002013000,
      paragraph: '2',
      articleOfCalculating: '2.13',
      name: '',

      parent: 1002000000,
      show_total: ' Всего по статье 2'
    }, {
      id: 1003000000,
      paragraph: '3',
      articleOfCalculating: '3',
      name: 'Статья 3. Проектные и изыскательские работы, в том числе:',
      calc: true,

    }, {
      id: 1003001000,
      paragraph: '3',
      articleOfCalculating: '3.1',
      name: 'инженерные изыскания',

      parent: 1003000000
    }, {
      id: 1003002000,
      paragraph: '3',
      articleOfCalculating: '3.2',
      name: 'разработка проекта (утверждаемая часть, рабочая документация)',

      parent: 1003000000
    }, {
      id: 1003003000,
      paragraph: '3',
      articleOfCalculating: '3.3',
      name: 'проведение экспертизы и согласование проекта (утверждаемая часть, рабочая документация)',

      parent: 1003000000
    }, {
      id: 1003004000,
      paragraph: '3',
      articleOfCalculating: '3.4',
      name: 'авторский надзор',

      parent: 1003000000
    }, {
      id: 1003005000,
      paragraph: '3',
      articleOfCalculating: '3.5',
      name: '',

      parent: 1003000000
    }, {
      id: 1003006000,
      paragraph: '3',
      articleOfCalculating: '3.6',
      name: '',

      parent: 1003000000
    }, {
      id: 1003007000,
      paragraph: '3',
      articleOfCalculating: '3.7',
      name: '',

      parent: 1003000000
    }, {
      id: 1003008000,
      paragraph: '3',
      articleOfCalculating: '3.8',
      name: '',

      parent: 1003000000
    }, {
      id: 1003009000,
      paragraph: '3',
      articleOfCalculating: '3.9',
      name: '',

      parent: 1003000000
    }, {
      id: 1003010000,
      paragraph: '3',
      articleOfCalculating: '3.10',
      name: '',

      parent: 1003000000
    }, {
      id: 1003011000,
      paragraph: '3',
      articleOfCalculating: '3.11',
      name: '',

      parent: 1003000000
    }, {
      id: 1003012000,
      paragraph: '3',
      articleOfCalculating: '3.12',
      name: '',

      parent: 1003000000
    }, {
      id: 1003013000,
      paragraph: '3',
      articleOfCalculating: '3.13',
      name: '',

      parent: 1003000000
    }, {
      id: 1003014000,
      paragraph: '3',
      articleOfCalculating: '3.14',
      name: '',

      parent: 1003000000,
      show_total: 'Всего по статье 3'
    }, {
      id: 1004000000,
      paragraph: '4',
      articleOfCalculating: '4',
      name: 'Статья 4. Внутриплощадочные сети инженерно-технического обеспечения и инженерные сооружения, в том числе:',
      calc: true,

    }, {
      id: 1004001000,
      paragraph: '4',
      articleOfCalculating: '4.1',
      name: 'объекты централизованной системы горячего водоснабжения',

      parent: 1004000000
    }, {
      id: 1004002000,
      paragraph: '4',
      articleOfCalculating: '4.2',
      name: 'объекты централизованной системы холодного водоснабжения',

      parent: 1004000000
    }, {
      id: 1004003000,
      paragraph: '4',
      articleOfCalculating: '4.3',
      name: 'объекты централизованной системы водоотведения',

      parent: 1004000000
    }, {
      id: 1004004000,
      paragraph: '4',
      articleOfCalculating: '4.4',
      name: 'объекты теплоснабжения',

      parent: 1004000000
    }, {
      id: 1004005000,
      paragraph: '4',
      articleOfCalculating: '4.5',
      name: 'объекты электросетевого хозяйства',

      parent: 1004000000
    }, {
      id: 1004006000,
      paragraph: '4',
      articleOfCalculating: '4.6',
      name: 'объекты систем газоснабжения',

      parent: 1004000000
    }, {
      id: 1004007000,
      paragraph: '4',
      articleOfCalculating: '4.7',
      name: 'линии связи',

      parent: 1004000000
    }, {
      id: 1004008000,
      paragraph: '4',
      articleOfCalculating: '4.8',
      name: '',

      parent: 1004000000
    }, {
      id: 1004009000,
      paragraph: '4',
      articleOfCalculating: '4.9',
      name: '',

      parent: 1004000000
    }, {
      id: 1004010000,
      paragraph: '4',
      articleOfCalculating: '4.10',
      name: '',

      parent: 1004000000
    }, {
      id: 1004011000,
      paragraph: '4',
      articleOfCalculating: '4.11',
      name: '',

      parent: 1004000000
    }, {
      id: 1004012000,
      paragraph: '4',
      articleOfCalculating: '4.12',
      name: '',

      parent: 1004000000
    }, {
      id: 1004013000,
      paragraph: '4',
      articleOfCalculating: '4.13',
      name: '',

      parent: 1004000000
    }, {
      id: 1004014000,
      paragraph: '4',
      articleOfCalculating: '4.14',
      name: '',

      parent: 1004000000
    }, {
      id: 1004015000,
      paragraph: '4',
      articleOfCalculating: '4.15',
      name: '',

      parent: 1004000000
    }, {
      id: 1004016000,
      paragraph: '4',
      articleOfCalculating: '4.16',
      name: '',

      parent: 1004000000
    }, {
      id: 1004017000,
      paragraph: '4',
      articleOfCalculating: '4.17',
      name: '',

      parent: 1004000000,
      show_total: 'Всего по статье 4'
    }, {
      id: 1005000000,
      paragraph: '5',
      articleOfCalculating: '5',
      name: 'Статья 5. Плата за подключение (технологическое присоединение) к сетям инженерно-технического обеспечения, в том числе:',
      calc: true,

    }, {
      id: 1005001000,
      paragraph: '5',
      articleOfCalculating: '5.1',
      name: 'объекты централизованной системы горячего водоснабжения',

      parent: 1005000000
    }, {
      id: 1005002000,
      paragraph: '5',
      articleOfCalculating: '5.2',
      name: 'объекты централизованной системы холодного водоснабжения',

      parent: 1005000000
    }, {
      id: 1005003000,
      paragraph: '5',
      articleOfCalculating: '5.3',
      name: 'объекты централизованной системы водоотведения',

      parent: 1005000000
    }, {
      id: 1005004000,
      paragraph: '5',
      articleOfCalculating: '5.4',
      name: 'объекты теплоснабжения',

      parent: 1005000000
    }, {
      id: 1005005000,
      paragraph: '5',
      articleOfCalculating: '5.5',
      name: 'объекты электросетевого хозяйства',

      parent: 1005000000
    }, {
      id: 1005006000,
      paragraph: '5',
      articleOfCalculating: '5.6',
      name: 'объекты систем газоснабжения',

      parent: 1005000000
    }, {
      id: 1005007000,
      paragraph: '5',
      articleOfCalculating: '5.7',
      name: 'линии связи',

      parent: 1005000000
    }, {
      id: 1005008000,
      paragraph: '5',
      articleOfCalculating: '5.8',
      name: '',

      parent: 1005000000
    }, {
      id: 1005009000,
      paragraph: '5',
      articleOfCalculating: '5.9',
      name: '',

      parent: 1005000000
    }, {
      id: 1005010000,
      paragraph: '5',
      articleOfCalculating: '5.10',
      name: '',

      parent: 1005000000
    }, {
      id: 1005011000,
      paragraph: '5',
      articleOfCalculating: '5.11',
      name: '',

      parent: 1005000000
    }, {
      id: 1005012000,
      paragraph: '5',
      articleOfCalculating: '5.12',
      name: '',

      parent: 1005000000
    }, {
      id: 1005013000,
      paragraph: '5',
      articleOfCalculating: '5.13',
      name: '',

      parent: 1005000000
    }, {
      id: 1005014000,
      paragraph: '5',
      articleOfCalculating: '5.14',
      name: '',

      parent: 1005000000
    }, {
      id: 1005015000,
      paragraph: '5',
      articleOfCalculating: '5.15',
      name: '',

      parent: 1005000000
    }, {
      id: 1005016000,
      paragraph: '5',
      articleOfCalculating: '5.16',
      name: '',

      parent: 1005000000
    }, {
      id: 1005017000,
      paragraph: '5',
      articleOfCalculating: '5.17',
      name: '',

      parent: 1005000000,
      show_total: 'Всего по статье 5'
    }, {
      id: 1006000000,
      paragraph: '6',
      articleOfCalculating: '6',
      name: 'Статья 6. Освоение территории, в том числе:',
      calc: true,

    }, {
      id: 1006001000,
      paragraph: '6',
      articleOfCalculating: '6.1',
      name: 'платежи по договорам об освоении территории в целях строительства стандартного жилья',

      parent: 1006000000
    }, {
      id: 1006002000,
      paragraph: '6',
      articleOfCalculating: '6.2',
      name: '',

      parent: 1006000000
    }, {
      id: 1006003000,
      paragraph: '6',
      articleOfCalculating: '6.3',
      name: '',

      parent: 1006000000
    }, {
      id: 1006004000,
      paragraph: '6',
      articleOfCalculating: '6.4',
      name: '',

      parent: 1006000000
    }, {
      id: 1006005000,
      paragraph: '6',
      articleOfCalculating: '6.5',
      name: '',

      parent: 1006000000
    }, {
      id: 1006006000,
      paragraph: '6',
      articleOfCalculating: '6.6',
      name: '',

      parent: 1006000000
    }, {
      id: 1006007000,
      paragraph: '6',
      articleOfCalculating: '6.7',
      name: '',

      parent: 1006000000
    }, {
      id: 1006008000,
      paragraph: '6',
      articleOfCalculating: '6.8',
      name: '',

      parent: 1006000000
    }, {
      id: 1006009000,
      paragraph: '6',
      articleOfCalculating: '6.9',
      name: '',

      parent: 1006000000
    }, {
      id: 1006010000,
      paragraph: '6',
      articleOfCalculating: '6.10',
      name: '',

      parent: 1006000000
    }, {
      id: 1006011000,
      paragraph: '6',
      articleOfCalculating: '6.11',
      name: '',

      parent: 1006000000,
      show_total: 'Всего по статье 6'
    }, {
      id: 1007000000,
      paragraph: '7',
      articleOfCalculating: '7',
      name: 'Статья 7. Развитие территории, в том числе:',
      calc: true,

    }, {
      id: 1007001000,
      paragraph: '7',
      articleOfCalculating: '7.1',
      name: 'платежи по договорам о развитии застроенной территории',

      parent: 1007000000
    }, {
      id: 1007002000,
      paragraph: '7',
      articleOfCalculating: '7.2',
      name: '',

      parent: 1007000000
    }, {
      id: 1007003000,
      paragraph: '7',
      articleOfCalculating: '7.3',
      name: '',

      parent: 1007000000
    }, {
      id: 1007004000,
      paragraph: '7',
      articleOfCalculating: '7.4',
      name: '',

      parent: 1007000000
    }, {
      id: 1007005000,
      paragraph: '7',
      articleOfCalculating: '7.5',
      name: '',

      parent: 1007000000
    }, {
      id: 1007006000,
      paragraph: '7',
      articleOfCalculating: '7.6',
      name: '',

      parent: 1007000000
    }, {
      id: 1007007000,
      paragraph: '7',
      articleOfCalculating: '7.7',
      name: '',

      parent: 1007000000
    }, {
      id: 1007008000,
      paragraph: '7',
      articleOfCalculating: '7.8',
      name: '',

      parent: 1007000000
    }, {
      id: 1007009000,
      paragraph: '7',
      articleOfCalculating: '7.9',
      name: '',

      parent: 1007000000
    }, {
      id: 1007010000,
      paragraph: '7',
      articleOfCalculating: '7.10',
      name: '',

      parent: 1007000000
    }, {
      id: 1007011000,
      paragraph: '7',
      articleOfCalculating: '7.11',
      name: '',

      parent: 1007000000,
      show_total: 'Всего по статье 7'
    }, {
      id: 1008000000,
      paragraph: '8',
      articleOfCalculating: '8',
      name: 'Статья 8. Комплексное освоение территории, в том числе:',
      calc: true,

    }, {
      id: 1008001000,
      paragraph: '8',
      articleOfCalculating: '8.1',
      name: 'платежи по договорам о комплексном освоении территории в целях строительства стандартного жилья',

      parent: 1008000000
    }, {
      id: 1008002000,
      paragraph: '8',
      articleOfCalculating: '8.2',
      name: 'объем средств, передаваемых на развитие социальной и инженерной инфраструктуры',

      parent: 1008000000
    }, {
      id: 1008003000,
      paragraph: '8',
      articleOfCalculating: '8.3',
      name: '',

      parent: 1008000000
    }, {
      id: 1008004000,
      paragraph: '8',
      articleOfCalculating: '8.4',
      name: '',

      parent: 1008000000
    }, {
      id: 1008005000,
      paragraph: '8',
      articleOfCalculating: '8.5',
      name: '',

      parent: 1008000000
    }, {
      id: 1008006000,
      paragraph: '8',
      articleOfCalculating: '8.6',
      name: '',

      parent: 1008000000
    }, {
      id: 1008007000,
      paragraph: '8',
      articleOfCalculating: '8.7',
      name: '',

      parent: 1008000000
    }, {
      id: 1008008000,
      paragraph: '8',
      articleOfCalculating: '8.8',
      name: '',

      parent: 1008000000
    }, {
      id: 1008009000,
      paragraph: '8',
      articleOfCalculating: '8.9',
      name: '',

      parent: 1008000000
    }, {
      id: 1008010000,
      paragraph: '8',
      articleOfCalculating: '8.10',
      name: '',

      parent: 1008000000
    }, {
      id: 1008011000,
      paragraph: '8',
      articleOfCalculating: '8.11',
      name: '',

      parent: 1008000000
    }, {
      id: 1008012000,
      paragraph: '8',
      articleOfCalculating: '8.12',
      name: '',

      parent: 1008000000,
      show_total: 'Всего по статье 8'
    }, {
      id: 1009000000,
      paragraph: '9',
      articleOfCalculating: '9',
      name: 'Статья 9. Затраты заказчика-застройщика, в том числе:',
      calc: true,

    }, {
      id: 1009001000,
      paragraph: '9',
      articleOfCalculating: '9.1',
      name: 'уплата процентов по целевым кредитам на строительство',

      parent: 1009000000
    }, {
      id: 1009001001,
      paragraph: '9',
      articleOfCalculating: '9.1.1',
      name: 'погашение основной суммы долга по целевым кредитам на строительство',

      parent: 9999999999
    }, {
      id: 1009002000,
      paragraph: '9',
      articleOfCalculating: '9.2',
      name: 'платежи, связанные с государственной регистрацией договоров участия в долевом строительстве',

      parent: 1009000000
    }, {
      id: 1009003000,
      paragraph: '9',
      articleOfCalculating: '9.3',
      name: 'платежи, связанные со страхованием ответственности застройщика',

      parent: 1009000000
    }, {
      id: 1009004000,
      paragraph: '9',
      articleOfCalculating: '9.4',
      name: 'уплата обязательных отчислений (взносов) в компенсационный фонд, предусмотренный частью 4 статьи 3 Закона о долевом строительстве',

      parent: 1009000000
    }, {
      id: 1009005000,
      paragraph: '9',
      articleOfCalculating: '9.5',
      name: 'оплата услуг уполномоченного банка по совершению операций с денежными средствами, находящимися на расчетном счете застройщика',

      parent: 1009000000
    }, {
      id: 1009006000,
      paragraph: '9',
      articleOfCalculating: '9.6',
      name: 'уплата налогов, сборов и иных обязательных взносов, уплачиваемых в бюджет соответствующего уровня',

      parent: 1009000000
    }, {
      id: 1009007000,
      paragraph: '9',
      articleOfCalculating: '9.7',
      name: 'оплата труда при условии одновременной уплаты соответствующих налогов, страховых взносов в Пенсионный фонд Российской Федерации, Фонд социального страхования Российской Федерации, Федеральный фонд обязательного медицинского страхования',

      parent: 1009000000
    }, {
      id: 1009008000,
      paragraph: '9',
      articleOfCalculating: '9.8',
      name: 'оплата услуг коммерческой организации, осуществляющей функции единоличного исполнительного органа застройщика',

      parent: 1009000000
    }, {
      id: 1009009000,
      paragraph: '9',
      articleOfCalculating: '9.9',
      name: 'денежные выплаты, связанные с предоставлением работникам гарантий и компенсаций, предусмотренных трудовым законодательством Российской Федерации',

      parent: 1009000000
    }, {
      id: 1009010000,
      paragraph: '9',
      articleOfCalculating: '9.10',
      name: '',

      parent: 1009000000
    }, {
      id: 1009011000,
      paragraph: '9',
      articleOfCalculating: '9.11',
      name: '',

      parent: 1009000000
    }, {
      id: 1009012000,
      paragraph: '9',
      articleOfCalculating: '9.12',
      name: '',

      parent: 1009000000
    }, {
      id: 1009013000,
      paragraph: '9',
      articleOfCalculating: '9.13',
      name: '',

      parent: 1009000000
    }, {
      id: 1009014000,
      paragraph: '9',
      articleOfCalculating: '9.14',
      name: '',

      parent: 1009000000
    }, {
      id: 1009015000,
      paragraph: '9',
      articleOfCalculating: '9.15',
      name: '',

      parent: 1009000000
    }, {
      id: 1009016000,
      paragraph: '9',
      articleOfCalculating: '9.16',
      name: '',

      parent: 1009000000
    }, {
      id: 1009017000,
      paragraph: '9',
      articleOfCalculating: '9.17',
      name: '',

      parent: 1009000000
    }, {
      id: 1009018000,
      paragraph: '9',
      articleOfCalculating: '9.18',
      name: '',

      parent: 1009000000
    }, {
      id: 1009019000,
      paragraph: '9',
      articleOfCalculating: '9.19',
      name: '',

      parent: 1009000000,
      show_total: 'Всего по статье 9'
    }, {
      id: 1010000000,
      paragraph: '10',
      articleOfCalculating: '10',
      name: 'Статья 10. Иные текущие расходы, в том числе:',
      calc: true,

    }, {
      id: 1010001000,
      paragraph: '10',
      articleOfCalculating: '10.1',
      name: 'реклама',

      parent: 1010000000
    }, {
      id: 1010002000,
      paragraph: '10',
      articleOfCalculating: '10.2',
      name: 'посреднические услуги',

      parent: 1010000000
    }, {
      id: 1010003000,
      paragraph: '10',
      articleOfCalculating: '10.3',
      name: 'затраты на текущее сопровождение строительства, включая сдачу объекта в эксплуатацию',

      parent: 1010000000
    }, {
      id: 1010004000,
      paragraph: '10',
      articleOfCalculating: '10.4',
      name: '',

      parent: 1010000000
    }, {
      id: 1010005000,
      paragraph: '10',
      articleOfCalculating: '10.5',
      name: '',

      parent: 1010000000
    }, {
      id: 1010006000,
      paragraph: '10',
      articleOfCalculating: '10.6',
      name: '',

      parent: 1010000000
    }, {
      id: 1010007000,
      paragraph: '10',
      articleOfCalculating: '10.7',
      name: '',

      parent: 1010000000
    }, {
      id: 1010008000,
      paragraph: '10',
      articleOfCalculating: '10.8',
      name: '',

      parent: 1010000000
    }, {
      id: 1010009000,
      paragraph: '10',
      articleOfCalculating: '10.9',
      name: '',

      parent: 1010000000
    }, {
      id: 1010010000,
      paragraph: '10',
      articleOfCalculating: '10.10',
      name: '',

      parent: 1010000000
    }, {
      id: 1010011000,
      paragraph: '10',
      articleOfCalculating: '10.11',
      name: '',

      parent: 1010000000
    }, {
      id: 1010012000,
      paragraph: '10',
      articleOfCalculating: '10.12',
      name: '',

      parent: 1010000000
    }, {
      id: 1010013000,
      paragraph: '10',
      articleOfCalculating: '10.13',
      name: '',

      parent: 1010000000,
      show_total: 'Всего по статье 10'
    }, {
      id: 1011000000,
      paragraph: '11',
      articleOfCalculating: '11',
      name: 'Статья 11. Авансы, оплаченные застройщиком в объеме, предусмотренном договором генерального подряда (подряда), в случае если указанным договором предусмотрена выплата аванса без разбивки выплачиваемых сумм по видам работ',

      // parent: 1010000000,
      show_total: 'Всего по статье 11'
    },
  ];

  isCalc = {};
  isVisible = {};
  canChangeName = {};
  showSum = {};

  sumByArticle = {};


  ngOnInit() {
    this.reportService.reportBuffer[0]['ConsolidatedCumulativeSheet'].shift();

    if (this.reportService.reportBuffer[0]['ConsolidatedCumulativeSheet'] !== null) {
      this.tableData = this.reportService.reportBuffer[0]['ConsolidatedCumulativeSheet'];
    } else {
      if (!this.tableData) {
        this.tableData = [];
      }
      let isPrevEmpty = false;

      this.articleOfCalculatingDictItems.forEach((row) => {
        if (row.calc) {
          this.isCalc[row.id] = true;
        }

        if (row.show_total) {
          this.showSum[row.id] = {
            name: row.show_total,
            key: row.articleOfCalculating.split('.').slice(0, -1).join('.'),
          };
        }

        this.tableData.push({
          rowType: {
            id: row.id,
            paragraph: row.paragraph,
            articleOfCalculating: row.articleOfCalculating,
            name: row.name,
          },
        });

        const isVisible = !isPrevEmpty || row.name !== '';

        if (isVisible) {
          this.isVisible[row.id] = true;
        }

        this.canChangeName[row.id] = row.name === '';
        isPrevEmpty = row.name === '';
      });

    }
    this.calcValues();
  }


  calcValues() {
    this.sumByArticle = {};
    let isPrevEmpty = false;

    this.tableData.forEach((row) => {

      if (!this.isCalc[row.rowType.id]) {
        this.saveCaclValues(row.rowType.articleOfCalculating, row);
      }

      const isVisible = !isPrevEmpty || row.rowType.name !== '';

      this.isVisible[row.rowType.id] = isVisible;

      isPrevEmpty = !(
        !!row.rowType.name || !!row.prevCost || !!row.totalCost ||
        !!row.paidWorks || !!row.assimilatedWorks || !!row.balanceForPayment
      );
    });

    this.tableData.forEach((row) => {
      if (this.isCalc[row.rowType.id]) {
        row.prevCost = this.sumByArticle[row.rowType.articleOfCalculating].prevCost.toFixed(2);
        row.totalCost = this.sumByArticle[row.rowType.articleOfCalculating].totalCost.toFixed(2);
        row.paidWorks = this.sumByArticle[row.rowType.articleOfCalculating].paidWorks.toFixed(2);
        row.assimilatedWorks = this.sumByArticle[row.rowType.articleOfCalculating].assimilatedWorks.toFixed(2);
        row.balanceForPayment = this.sumByArticle[row.rowType.articleOfCalculating].balanceForPayment.toFixed(2);
      }
    });

  }

  saveCaclValues(articleOfCalculating, row) {
    let calcKey = articleOfCalculating.split('.').slice(0, -1).join('.');
    if (articleOfCalculating === '') {
      calcKey = 'total';
    }

    if (!this.sumByArticle[calcKey]) {
      this.sumByArticle[calcKey] = {
        prevCost: 0,
        totalCost: 0,
        paidWorks: 0,
        assimilatedWorks: 0,
        balanceForPayment: 0,
      };
    }

    this.sumByArticle[calcKey].prevCost += +row.prevCost || 0;
    this.sumByArticle[calcKey].totalCost += +row.totalCost || 0;
    this.sumByArticle[calcKey].paidWorks += +row.paidWorks || 0;
    this.sumByArticle[calcKey].balanceForPayment += +row.balanceForPayment || 0;
    this.sumByArticle[calcKey].assimilatedWorks += +row.assimilatedWorks || 0;

    if (calcKey !== 'total') {
      this.saveCaclValues(calcKey, row);
    }
  }

  validateLongFloat(event) {
    if (event.target.value !== '') {
      event.target.value = parseFloat(event.target.value).toFixed(2);
    }
  }

  public isValid() {
    let isValid = true;
    this.showErrorPrevCost = false;
    this.showErrorTotalCost = false;
    this.showErrorPaidWorks = false;
    this.showErrorAssimilatedWorks = false;
    this.showErrorBalanceForPayment = false;

    // @ts-ignore
    if (!isStringValid(this.sumByArticle.total.prevCost) || this.sumByArticle.total.prevCost === 0) {
      this.showErrorPrevCost = true;
      isValid = false;
    }
    // @ts-ignore
    if (!isStringValid(this.sumByArticle.total.totalCost) || this.sumByArticle.total.totalCost === 0) {
      this.showErrorTotalCost = true;
      isValid = false;
    }
    // @ts-ignore
    if (!isStringValid(this.sumByArticle.total.paidWorks) || this.sumByArticle.total.paidWorks === 0) {
      this.showErrorPaidWorks = true;
      isValid = false;
    }
    // @ts-ignore
    if (!isStringValid(this.sumByArticle.total.assimilatedWorks) || this.sumByArticle.total.assimilatedWorks === 0) {
      this.showErrorAssimilatedWorks = true;
      isValid = false;
    }
    // @ts-ignore
    if (!isStringValid(this.sumByArticle.total.balanceForPayment) || this.sumByArticle.total.balanceForPayment === 0) {
      this.showErrorBalanceForPayment = true;
      isValid = false;
    }

    return isValid;
  }
}
