import { Component, Input, OnInit } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap';
import * as _ from 'lodash';

import { Report } from '../models/report.model';

@Component({
  selector: 'app-rates-h1',
  templateUrl: './rates-h1.component.html',
})
export class RatesH1Component implements OnInit {
  @Input() report: Report = new Report();

  loading = false;
  showErrors = false;

  measuringUnits = [
    {
      code: 'RUBLES',
      name: 'рубли'
    },
    {
      code: 'THOUSANDS_OF_RUBLES',
      name: 'тыс. рублей'
    }
  ];

  constructor(private modalService: BsModalService) {
  }

  ngOnInit() {
    if (this.report.buildingOutsideOfMoscow === undefined) {
      this.report.buildingOutsideOfMoscow = 'false';
    }

    this.report.b1600 = this.getActualValueByCode('1600');

    if (!this.report.measuringUnit) {
      this.report.measuringUnit = 'RUBLES';
    }

    this.setUpB16000();
    this.calcH1();
  }

  setUpB16000() {
    if (this.report.b16000) {
      return;
    }

    this.report.b16000 = 0;
    this.report.Appendix2s.forEach((appendix) => {
      this.report.b16000 = this.report.b16000 + +appendix.sumObligation;
    });

    if (this.report.measuringUnit === 'RUBLES') {
      this.report.b16000 *= 1000000;
    } else {
      this.report.b16000 *= 1000;
    }
  }

  onMeasuringUnitsChange(val) {
    if (val === 'THOUSANDS_OF_RUBLES') {
      this.report.b16000 /= 1000;
    } else {
      this.report.b16000 *= 1000;
    }
    this.calcH1();
  }

  calcH1() {
    if (!this.report.b16000 || this.report.b16000.toString().trim() === '' || this.report.b16000 === 0) {
      this.report.h1 = '';
    }

    this.report.h1 = (this.report.b1600 / this.report.b16000).toFixed(2);
    // @ts-ignore
    if (this.report.h1 === 'Infinity') {
      this.report.h1 = '';
    }
  }

  getActualValueByCode(code: string) {
    if (+code.slice(0, 2) > 12 && code !== '1600') {
      return this.getActualValue(this.getLiabilityRowByCode(code));
    } else {
      return this.getActualValue(this.getAssetRowByCode(code));
    }
  }

  getActualValue(row) {
    return row.values.find((value) => (value.quarter === +this.report.ikvartal && value.year === +this.report.year)).value;
  }

  getAssetRowByCode(code) {
    return this.report.balanceNodes.assets.find((node) => (node.code === code));
  }

  getLiabilityRowByCode(code) {
    return this.report.balanceNodes.liabilities.find((node) => (node.code === code));
  }

  public isValid() {
    if (!this.report.balanceNodes.assets) {
      return false;
    }

    if (!this.report.balanceNodes.liabilities) {
      return false;
    }

    if (!this.report.balanceNodes.report) {
      return false;
    }

    return true;
  }
}
