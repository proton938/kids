import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import * as _ from 'lodash';

import { Report, BalanceNode } from '../models/report.model';

@Component({
  selector: 'app-rates-balance-table',
  templateUrl: './rates-balance-table.component.html',
  styleUrls: ['./rates-balance-table.component.scss']
})
export class RatesBalanceTableComponent implements OnInit {
  @Input() tableCode: any;
  @Input() report: any;
  @Output() onChange: EventEmitter<any> = new EventEmitter();

  quarterLastDayMap = {
    1: '31 марта',
    2: '30 июня',
    3: '30 сентября',
    4: '31 декабря',
  };

  titles = {
    1110: 'I. ВНЕОБОРОТНЫЕ АКТИВЫ',
    1210: 'II. ОБОРОТНЫЕ АКТИВЫ',
    1310: 'III. КАПИТАЛ И РЕЗЕРВЫ',
    1410: 'IV. ДОЛГОСРОЧНЫЕ ОБЯЗАТЕЛЬСТВА',
    1510: 'V. КРАТКОСРОЧНЫЕ ОБЯЗАТЕЛЬСТВА'
  };

  additionalFields = [
    'unfinishedBuildingActivities',
    'contractDebts',
    'contractLiabilities',
    'debtOfFounders',
    'revenueOfFuturePeriods'
  ];

  hasunfinishedBuildingActivities = false;
  hascontractDebts = false;
  hascontractLiabilities = false;
  hasdebtOfFounders = false;
  hasrevenueOfFuturePeriods = false;

  checkboxMatrix = {};

  checkRules = {
    unfinishedBuildingActivities: [
      {
        parent: '1150',
        excludeParent: true,
      }, {
        parent: '1190',
        excludeParent: true,
      }
    ],
    contractDebts: [
      {
        parent: '1230',
        excludeParent: true,
      }
    ],
    contractLiabilities: [
      {
        parent: '1440'
      }, {
        parent: '1450'
      }, {
        parent: '1460',
        excludeChildren: true,
      }, {
        parent: '1520',
        excludeParent: true,
      }, {
        parent: '1530'
      }, {
        parent: '1550'
      }
    ],
    debtOfFounders: [
      {
        parent: '1230',
        excludeParent: true,
      }
    ],
    revenueOfFuturePeriods: [
      {
        parent: '1530',
        excludeParent: true,
      }
    ],
  };

  valuesColumnName: string;

  tableData: any[];
  tableTitle: string;

  setAdditionalFields(row, rowIdx) {
    this.additionalFields.forEach((field) => {
      const rules = this.checkRules[field];

      if (!rules) {
        return;
      }

      rules.forEach((rule) => {
        const parentCodeStart = rule.parent.replace(/(0+)$/g, '');

        if (row.code.startsWith(parentCodeStart)) {
          if (row.code === rule.parent && rule.excludeParent) {
            // ignore
          }
          if (row.code === rule.parent && !rule.excludeParent) {
            this.checkboxMatrix[rowIdx].push(field);
          }

          if (row.code !== rule.parent && rule.excludeChildren) {
            // ignore
          }

          if (row.code !== rule.parent && !rule.excludeChildren) {
            this.checkboxMatrix[rowIdx].push(field);
          }
        }
      });
    });
  }

  ngOnInit() {
    this.tableData = this.report.balanceNodes[this.tableCode];
    this.tableTitle = this.tableCode === 'assets' ? 'Активы' : 'Пассивы';
    this.checkboxMatrix = Array(this.tableData.length).fill([]).map((e) => ([]));
    this.valuesColumnName = this.generateTableValuesColumnName(this.tableData[0]);
    this.tableData.forEach(this.setAdditionalFields.bind(this));

    this.calsDeps();
  }

  calsDeps() {
    this.tableData.forEach((row) => {
      this.additionalFields.forEach((field) => {
        this[`has${field}`] = this[`has${field}`] || false;

        if (row[field]) {
          this[`has${field}`] = true;
        }
      });
    });
  }

  getValueByPeriod(balanceNode: BalanceNode) {
    return balanceNode.values.find((value) => (+value.quarter === +this.report.ikvartal && +value.year === +this.report.year)).value;
  }

  generateTableValuesColumnName(row: any) {
    let valuesColumnName = '';

    row.values.some((value) => {
      if (+value.quarter === +this.report.ikvartal && +value.year === +this.report.year) {
        valuesColumnName = `На ${this.quarterLastDayMap[+value.quarter]} ${value.year} г.`;
        return true;
      }
    });

    return valuesColumnName;
  }

  onCheckboxChange() {
    this.calsDeps();

    this.onChange.emit();
  }
}
