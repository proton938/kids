import { Component, Input, OnInit, Output, EventEmitter, ViewChild, ElementRef, AfterViewInit, AfterViewChecked } from '@angular/core';
import * as _ from 'lodash';
import * as $ from 'jquery';

import { BalanceNode } from '../models/report.model';

@Component({
  selector: 'app-rates-nss-balance-table',
  templateUrl: './rates-nss-balance-table.component.html',
  styleUrls: ['./rates-nss-balance-table.component.scss']
})
export class RatesNSSBalanceTableComponent implements OnInit, AfterViewInit, AfterViewChecked {
  @Input() tableCode: any;
  @Input() report: any;
  @Output() onChange: EventEmitter<any> = new EventEmitter();

  @ViewChild('tableFixed', null) tableFixed: ElementRef;
  @ViewChild('tableVariable', null) tableVariable: ElementRef;

  quarterLastDayMap = {
    1: '31 марта',
    2: '30 июня',
    3: '30 сентября',
    4: '31 декабря',
  };

  titles = {
    1110: 'I. ВНЕОБОРОТНЫЕ АКТИВЫ',
    1210: 'II. ОБОРОТНЫЕ АКТИВЫ',
    1310: 'III. КАПИТАЛ И РЕЗЕРВЫ',
    1410: 'IV. ДОЛГОСРОЧНЫЕ ОБЯЗАТЕЛЬСТВА',
    1510: 'V. КРАТКОСРОЧНЫЕ ОБЯЗАТЕЛЬСТВА'
  };

  additionalFields = [
    'unfinishedBuildingActivities673',
    'taxesAndFees673',
    'finishedBuildingActivities673',
    'prepayment673',
    'otherCosts673',
    'contractDebts673',
    'valueAddedTax673',
    'cashInBankAccounts673',
    'revenueOfFuturePeriods673'
  ];

  checkboxMatrix = {};

  checkRules = {
    unfinishedBuildingActivities673: [
      {
        parent: '1150',
        excludeParent: true
      }, {
        parent: '1190',
        excludeParent: true
      }, {
        parent: '1260',
        excludeParent: true
      }
    ],
    taxesAndFees673: [
      {
        parent: '1230',
        excludeParent: true
      }
    ],
    finishedBuildingActivities673: [
      {
        parent: '1150'
      }, {
        parent: '1210'
      }
    ],
    prepayment673: [
      {
        parent: '1230',
        excludeParent: true
      }
    ],
    otherCosts673: [
      {
        parent: '1230',
        excludeParent: true
      }, {
        parent: '1260',
        excludeParent: true
      }
    ],
    contractDebts673: [
      {
        parent: '1230',
        excludeParent: true
      }, {
        parent: '1260',
        excludeParent: true
      }],
    valueAddedTax673: [{
      parent: '1220'
    }],
    cashInBankAccounts673: [{
      parent: '1230',
      excludeParent: true
    }, {
      parent: '1250'
    }
    ],
    revenueOfFuturePeriods673: [
      {
        parent: '1530',
        excludeParent: true
      }
    ]
  };

  valuesColumnName: string;

  tableData: any[];
  tableTitle: string;

  syncTables = _.throttle(() => {
    const fixedRows = $(this.tableFixed.nativeElement).find('tr');
    const variableRows = $(this.tableVariable.nativeElement).find('tr');

    fixedRows.each((idx, td) => {
      const fixedRow = $(td);
      const fixedHeight = fixedRow.height();
      const variableRow = $($(variableRows[idx])[0]);
      const variableHeight = variableRow.height();
      // console.log('height fix', fixedRow.height(), fixedRow.innerHeight(), ' var', variableRow.height(), variableRow.innerHeight());
      if (fixedHeight > variableHeight) {
        variableRow.find('.first-cell').css('height', `${$(td).height()}px`);
      } else if (fixedHeight < variableHeight) {
        fixedRow.find('.first-cell').css('height', `${variableHeight}px`);
      }
    });
  }, 300).bind(this);

  setAdditionalFields(row, rowIdx) {
    this.additionalFields.forEach((field) => {
      const rules = this.checkRules[field];

      if (!rules) {
        return;
      }

      rules.forEach((rule) => {
        const parentCodeStart = rule.parent.replace(/(0+)$/g, '');

        if (row.code.startsWith(parentCodeStart)) {
          if (row.code === rule.parent && rule.excludeParent) {
            // ignore
          }
          if (row.code === rule.parent && !rule.excludeParent) {
            this.checkboxMatrix[rowIdx].push(field);
          }

          if (row.code !== rule.parent && rule.excludeChildren) {
            // ignore
          }

          if (row.code !== rule.parent && !rule.excludeChildren) {
            this.checkboxMatrix[rowIdx].push(field);
          }
        }
      });
    });
  }

  ngOnInit() {
    this.tableData = this.report.balanceNodes[this.tableCode];
    this.tableTitle = this.tableCode === 'assets' ? 'Активы' : 'Пассивы';
    this.checkboxMatrix = Array(this.tableData.length).fill([]).map((e) => ([]));
    this.valuesColumnName = this.generateTableValuesColumnName(this.tableData[0]);
    this.tableData.forEach(this.setAdditionalFields.bind(this));

    this.calsDeps();
  }

  ngAfterViewInit() {
   this.syncTables();
  }

  ngAfterViewChecked() {
    this.syncTables();
  }

  calsDeps() {
    this.tableData.forEach((row) => {
      this.additionalFields.forEach((field) => {
        this[`has${field}`] = this[`has${field}`] || false;

        if (row[field]) {
          this[`has${field}`] = true;
        }
      });
    });
  }

  getValueByPeriod(balanceNode: BalanceNode) {
    return balanceNode.values.find((value) => (+value.quarter === +this.report.ikvartal && +value.year === +this.report.year)).value;
  }

  generateTableValuesColumnName(row: any) {
    let valuesColumnName = '';

    row.values.some((value) => {
      if (+value.quarter === +this.report.ikvartal && +value.year === +this.report.year) {
        valuesColumnName = `На ${this.quarterLastDayMap[+value.quarter]} ${value.year} г.`;
        return true;
      }
    });

    return valuesColumnName;
  }

  onCheckboxChange() {
    this.calsDeps();

    this.onChange.emit();
  }
}
