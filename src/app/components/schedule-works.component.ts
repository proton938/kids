
import { Component, Input, EventEmitter } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap';

import { ScheduleWork, ScheduleWorkItem } from '../models/report.model';
import { ScheduleWorkComponent } from '../modals/schedule-work-component';

@Component({
  selector: 'app-schedule-works',
  templateUrl: './schedule-works.component.html',
  styleUrls: ['./schedule-works.component.scss']
})
export class ScheduleWorksComponent {
  @Input() scheduleWorks: ScheduleWork[];
  @Input() ikvartal: string;
  @Input() year: string;

  showErrors = false;
  public event: EventEmitter<any> = new EventEmitter();

  constructor(private modalService: BsModalService) {
  }

  hasErrors(scheduleWork) {
    const scheduleWorkItems: ScheduleWorkItem[] = scheduleWork.ScheduleWorkItemss;

    if (scheduleWorkItems.length === 0) {
      return true;
    }

    if (scheduleWork.workType.value === 70012) {
      const hasNotFilledDate = scheduleWorkItems.some((scheduleWorkItem: ScheduleWorkItem) => {
        return !scheduleWorkItem.permission_plandate;
      });

      if (hasNotFilledDate) {
        return true;
      }

      return false;
    }

    if (scheduleWork.workType.value === 70011) {
      const hasNotFilledDate = scheduleWorkItems.some((scheduleWorkItem: ScheduleWorkItem) => {
        return !scheduleWorkItem.conformity_plandate;
      });

      if (hasNotFilledDate) {
        return true;
      }

      return false;
    }

    if (scheduleWork.workType.value === 70013) {
      const hasNotFilledDate = scheduleWorkItems.some((scheduleWorkItem: ScheduleWorkItem) => {
        return !scheduleWorkItem.transfer_plandate;
      });

      if (hasNotFilledDate) {
        return true;
      }

      return false;
    }

    const hasCurrentPeriodOrAlreadyFinished = scheduleWorkItems.some((scheduleWorkItem: ScheduleWorkItem) => {
      if (+scheduleWorkItem.plany < +this.year) {
        return +scheduleWorkItem.factpc === 100;
      } else if (+scheduleWorkItem.plany === +this.year) {
        if (+scheduleWorkItem.planq < +this.ikvartal) {
          return +scheduleWorkItem.factpc === 100;
        } else if (+scheduleWorkItem.planq === +this.ikvartal) {
          return true;
        }
      }
    });

    return !hasCurrentPeriodOrAlreadyFinished;
  }

  edit(index) {
    const initialState = {
      scheduleWork: JSON.parse(JSON.stringify(this.scheduleWorks[index])),
      ikvartal: this.ikvartal,
      year: this.year,
    };
    const modal = this.modalService.show(ScheduleWorkComponent, { class: 'modal-lg child-modal', initialState });

    modal.content.event.subscribe((scheduleWork) => {
      this.scheduleWorks[index] = scheduleWork;
    });
  }
}
