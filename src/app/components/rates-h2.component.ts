import { Component, Input, OnInit } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap';
import * as _ from 'lodash';

import { Report } from '../models/report.model';
import { ErrorComponent } from 'src/app/modals/error.component';

@Component({
  selector: 'app-rates-h2',
  templateUrl: './rates-h2.component.html',
  styleUrls: ['./rates-h2.component.scss']
})
export class RatesH2Component implements OnInit {

  constructor(private modalService: BsModalService) {
  }
  @Input() report: Report = new Report();

  loading = false;
  showErrors = false;

  b1100Row: any;
  b1230Row: any;
  b1240Row: any;
  b1400Row: any;
  b1500Row: any;
  b1600Row: any;

  b1100: any;
  b1230: any;
  b1240: any;
  b1400: any;
  b1500: any;
  b1600: any;

  unfinishedBuildingActivities = 0;
  contractDebts = 0;
  contractLiabilities = 0;
  debtOfFounders = 0;
  revenueOfFuturePeriods = 0;

  unfinishedBuildingActivitiesRows = [];
  contractDebtsRows = [];
  contractLiabilitiesRows = [];
  debtOfFoundersRows = [];
  revenueOfFuturePeriodsRows = [];

  ngOnInit() {
    this.calcH2();
  }

  getActualValueByCode(code: string) {
    if (+code.slice(0, 2) > 12 && code !== '1600') {
      return this.getActualValue(this.getLiabilityRowByCode(code));
    } else {
      return this.getActualValue(this.getAssetRowByCode(code));
    }
  }

  getActualValue = (row) => {
    return row.values.find((value) => (value.quarter === +this.report.ikvartal && value.year === +this.report.year)).value;
  }

  getRowByCode(code) {
    if (+code.slice(0, 2) > 12 && code !== '1600') {
      return this.getLiabilityRowByCode(code);
    } else {
      return this.getAssetRowByCode(code);
    }
  }

  getRowsByField(field, value) {
    return [
      ...this.report.balanceNodes.assets.filter((node) => (node[field] === value)),
      ...this.report.balanceNodes.liabilities.filter((node) => (node[field] === value)),
    ];
  }

  getAssetRowByCode(code) {
    return this.report.balanceNodes.assets.find((node) => (node.code === code));
  }

  getLiabilityRowByCode(code) {
    return this.report.balanceNodes.liabilities.find((node) => (node.code === code));
  }

  calcH2() {
    this.unfinishedBuildingActivities = 0;
    this.contractDebts = 0;
    this.contractLiabilities = 0;
    this.debtOfFounders = 0;
    this.revenueOfFuturePeriods = 0;

    this.unfinishedBuildingActivitiesRows = [];
    this.contractDebtsRows = [];
    this.contractLiabilitiesRows = [];
    this.debtOfFoundersRows = [];
    this.revenueOfFuturePeriodsRows = [];

    this.b1100 = this.getActualValueByCode('1100');
    this.b1100Row = this.getRowByCode('1100');

    this.b1230 = this.getActualValueByCode('1230');
    this.b1230Row = this.getRowByCode('1230');

    this.b1240 = this.getActualValueByCode('1240');
    this.b1240Row = this.getRowByCode('1240');

    this.b1400 = this.getActualValueByCode('1400');
    this.b1400Row = this.getRowByCode('1400');

    this.b1500 = this.getActualValueByCode('1500');
    this.b1500Row = this.getRowByCode('1500');

    this.b1600 = this.getActualValueByCode('1600');
    this.b1600Row = this.getRowByCode('1600');

    if (this.report.withoutUnfinishedBuildingActivities !== true) {
      this.unfinishedBuildingActivitiesRows = this.getRowsByField('unfinishedBuildingActivities', true);
      this.unfinishedBuildingActivities = _.sumBy(this.unfinishedBuildingActivitiesRows, this.getActualValue);
    }

    if (this.report.withoutRevenueOfFuturePeriods !== true) {
      this.revenueOfFuturePeriodsRows = this.getRowsByField('revenueOfFuturePeriods', true);
      this.revenueOfFuturePeriods = _.sumBy(this.revenueOfFuturePeriodsRows, this.getActualValue);
    }

    if (this.report.withoutDebtOfFounders !== true) {
      this.debtOfFoundersRows = this.getRowsByField('debtOfFounders', true);
      this.debtOfFounders = _.sumBy(this.debtOfFoundersRows, this.getActualValue);
    }

    if (this.report.withoutContractLiabilities !== true) {
      this.contractLiabilitiesRows = this.getRowsByField('contractLiabilities', true);
      this.contractLiabilities = _.sumBy(this.contractLiabilitiesRows, this.getActualValue);
    }

    if (this.report.withoutContractDebts !== true) {
      this.contractDebtsRows = this.getRowsByField('contractDebts', true);
      this.contractDebts = _.sumBy(this.contractDebtsRows, this.getActualValue);
    }

    const an = this.b1100 - this.unfinishedBuildingActivities + (this.b1230 - this.contractDebts) + this.b1240;
    const d = this.b1400 + (this.b1500 - this.contractLiabilities);

    this.report.h2 = an / (d + (this.b1600 - this.debtOfFounders - (this.b1400 + this.b1500 - this.revenueOfFuturePeriods)));

    this.report.h2 = this.report.h2.toFixed(2);
  }
}
