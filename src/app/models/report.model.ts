import _ = require('lodash');

export class Appendix2 {
  realEstateKind: string;
  placeObject: string;
  cadastralNumber: string[] = [];
  rightKind: string;
  contractNumber: string;
  contractDate: string;
  endRightDate: string;
  egrnDate: string;
  numberOfLivingQuarters: number;
  areaOfLivingQuarters: number; // double
  numberOfNonLivingQuarters: number;
  areaOfNonLivingQuarters: number; // double
  numberOfCarPark: number;
  areaOfCarPark: number; // double
  numberOfIndParts = 0; // numberOfLivingQuarters + numberOfNonLivingQuarters + numberOfCarPark
  areaOfIndParts = 0; // areaOfLivingQuarters + areaOfNonLivingQuarters + areaOfCarPark
  estimatedCost: number;
  projectCost: number;
  changeCost: number;
  reportCost: number;
  sumCost: number;
  totalCost: number;
  contractorsCost: number;
  developerCost: number;
  paidStockCost: number;
  infBorrowing = 'false';
  balanceOfBorrowed: number;
  amountMoneyParticipants: number;
  totalAmountCash: number;
  totalCashCompensation: number;
  totalCashCompensationFirstDate: number;
  totalCashReturnedFirstDate: number;
  totalCashReturned: number;
  balance: number;
  startDate: string;
  amountContractsConcludedDuringRepPer: number;
  amountContractsDivorcedDuringRepPer: number;
  contractsToReportDate: number;
  livingObjectsToReportDate: number;
  areaLivingObjectsToReportDate: number;
  nonLivingObjectsToReportDate: number;
  areaNonLivingObjectsToReportDate: number;
  numberOfCarParkToReportDate: number;
  areaOfCarParkToReportDate: number;
  sumObligation: number;
  sumObligationDdu: number;
  accountsReceivable: number;
  totalAmountAttracted: number;
  amountUsedContract = 0;
  amountUnUsedContract: number;
  totalAmountOutstanding: number;
  firstTimeTransfer: string;
  timeTransfer: string;
  permissionCommision = 'false';
  partiallyEntered = 'false';
  permCommissionNumber: string;
  permCommissionDate: string;
  permCommissionDistributor: string;
  compensationFundAmount: number;
  citizensRightsFundAmount: number;
  bankLegalFormOfOrganization: string;
  checkingAccount: string;

  buildingPermits: BuildingPermit[] = [];

  InsuranceAgreements: InsuranceAgreement[] = [];
  creditOrganizations: CreditOrganization[] = [];
  loanCompanies: LoanCompany[] = [];
  insuranceAgreementAbsent = false;

  bankGuarantorAbsent = false;
  bankGuarantorName: string;
  bankGuarantorINN: string;
  bankGuarantorPlace: string;
  contractGuaranteeDate: string;
  contractGuaranteeNumber: string;
  contractGuaranteeEndDate: string;
  contractGuaranteeNewDate: string;

  ScheduleWorks: ScheduleWork[] = [
    { workType: { value: 70001, name: 'Подготовительные, геодезические работы' }, ScheduleWorkItemss: [] },
    { workType: { value: 70002, name: 'Земляные работы' }, ScheduleWorkItemss: [] },
    { workType: { value: 70003, name: 'Работы при возведении конструкций фундаментов' }, ScheduleWorkItemss: [] },
    { workType: { value: 70004, name: 'Работы при возведении конструкций подземной части' }, ScheduleWorkItemss: [] },
    { workType: { value: 70005, name: 'Работы при возведении конструкций надземной части' }, ScheduleWorkItemss: [] },
    { workType: { value: 70006, name: 'Работы при устройстве внутридомовых сетей инженерно-технического обеспечения' }, ScheduleWorkItemss: [] },
    { workType: { value: 70007, name: 'Работы при устройстве внутриплощадочных сетей инженерно-технического обеспечения' }, ScheduleWorkItemss: [] },
    { workType: { value: 70008, name: 'Работы по благоустройству объекта недвижимости' }, ScheduleWorkItemss: [] },
    { workType: { value: 70010, name: 'Степень готовности объекта недвижимости (%)' }, ScheduleWorkItemss: [] },
    { workType: { value: 70012, name: 'Получение заключения о соответствии (дата)' }, ScheduleWorkItemss: [] },
    { workType: { value: 70011, name: 'Получение разрешения на ввод объекта в эксплуатацию (дата)' }, ScheduleWorkItemss: [] },
    { workType: { value: 70013, name: 'Срок передачи объектов долевого строительства участникам долевого строительства' }, ScheduleWorkItemss: [] },
  ];
}

export class ScheduleWork {
  workType: WorkType;
  ScheduleWorkItemss: ScheduleWorkItem[] = [];
}

export class WorkType {
  value: number;
  name: string;
}

export class ScheduleWorkItem {
  planq?: any;
  plany?: any;
  planpc?: any;
  factpc?: any;
  permission_plandate?: string;
  permission_factdate?: string;
  conformity_plandate?: string;
  conformity_factdate?: string;
  transfer_plandate?: string;
  transfer_factdate?: string;
  transfer_finish?: string;
}

export class InsuranceAgreement {
  insuranceOrganizationName: string;
  insuranceOrganizationPlace: string;
  insuranceOrganizationINN: string;
  insuranceContractDate: string;
  insuranceContractNumber: string;
  validityOfInsuranceContract: string;
  newValidityOfInsuranceContract: string;
}

export class LoanCompany {
  loanCompanyName: string;
  loanCompanyINN: string;
  totalBorrowedFunds: string;
}

export class BuildingPermit {
  permitNumber: string;
  permitDate: string;
  permitDistributor: string;
  periodOfValidity: string;
  lastProlongationPermitDate: string;
  description: string;
  newDate: string;
}

export class CreditOrganization {
  creditOrganizationName: string;
  creditOrganizationINN: string;
  amountBorrowing: string;
  beforeBorrowing: string;
  afterBorrowing: string;
}

export class Report {
  ikvartal: number = 1; // 1 2 3 4
  year: string = '2020';

  organizationName: string;
  shortName: string;

  devIndex: string;
  devSubject: string;
  devDistrict?: string;
  devSettlementKind?: string;
  devSettlementName?: string;
  devRoadNetworkElement?: string;
  devRoadNetworkElementName?: string;
  devBuildingType?: string;
  devBuildingNumber?: string;
  devBuildingType2?: string;
  devBuildingNumber2?: string;
  devNumber?: string;
  seaIsOrganization: boolean | string = 'true';
  seaOrganizationName?: string;
  seaOrganizationINN?: string;
  seaPersonFirstName?: string;
  seaPersonLastName?: string;
  seaPersonMiddleName?: string;
  OGRN?: string;
  regDate?: Date;
  INN?: string;
  buildingOutsideOfMoscow: boolean | string = 'false';
  h1: number | string;
  h2: number | string;

  pf: number | string;
  normPf: number | string;
  b1600: number;
  b16000: number;
  measuringUnit = 'RUBLES';

  Appendix2s: Appendix2[] = [];
  balanceNodes: {
    assets?: BalanceNode[];
    liabilities?: BalanceNode[];
    report?: BalanceNode[];
  } = {};
  withoutUnfinishedBuildingActivities: boolean | string = false;
  withoutContractDebts: boolean | string = false;
  withoutContractLiabilities: boolean | string = false;
  withoutDebtOfFounders: boolean | string = false;
  withoutRevenueOfFuturePeriods: boolean | string = false;

  withoutUnfinishedBuildingActivities673 = false;
  withoutTaxesAndFees673 = false;
  withoutFinishedBuildingActivities673 = false;
  withoutPrepayment673 = false;
  withoutOtherCosts673 = false;
  withoutContractDebts673 = false;
  withoutValueAddedTax673 = false;
  withoutCashInBankAccounts673 = false;
  withoutRevenueOfFuturePeriods673 = false;
  withoutDeferredTaxLiabilities673 = false;

  obligationsRef: {
    items: ObligationsRefItem[];
  };
  obligationsRefCount: number;

  otherCostsRef: {
    items: OtherCostsRefItem[];
  };
  otherCostsRefCount: number;

  consolidatedCumulativeSheets: ConsolidatedCumulativeSheet[];
}

export class ConsolidatedCumulativeSheet {
  SheetBuildingPermit: BuildingPermit;
  sheetBuildingProject: string;
  sheetBuildingAddress: string;
  sheetBuildingArea: string;
  sheetConstructionTime: string;
  sheetRow: SheetRow[];
}

export class SheetRow {
  rowType: ArticleOfCalculating;
  prevCost?: string;
  totalCost?: string;
  paidWorks?: string;
  assimilatedWorks?: string;
  balanceForPayment?: string;
}

export class ArticleOfCalculating {
  id: string | number;
  paragraph: string;
  articleOfCalculating: string;
  name: string;
}

export class OtherCostsRefItem {
  name: string;
  sum: string;
  pdLink: string;
}

export class ObligationsRefItem {
  oAddress: string;
  tDate: string;
  reason: string;
}

export class BalanceNode {
  code: string;
  contractDebts: boolean;
  contractLiabilities: boolean;
  debtOfFounders: boolean;
  name: string;
  revenueOfFuturePeriods: boolean;
  unfinishedBuildingActivities: boolean;
  values: BalanceNodeValue[];

  unfinishedBuildingActivities673: boolean;
  taxesAndFees673: boolean;
  finishedBuildingActivities673: boolean;
  prepayment673: boolean;
  otherCosts673: boolean;
  contractDebts673: boolean;
  valueAddedTax673: boolean;
  cashInBankAccounts673: boolean;
  revenueOfFuturePeriods673: boolean;
  deferredTaxLiabilities673: boolean;
}

export class BalanceNodeValue {
  quarter: string | number;
  value: number;
  year: string | number;
}
