import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { APP_BASE_HREF } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { LaddaModule } from 'angular2-ladda';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgWizardModule, NgWizardConfig, THEME } from 'ng-wizard';
import { ModalModule } from 'ngx-bootstrap/modal';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { NgxMaskModule } from 'ngx-mask';
import { BlockUIModule } from 'ng-block-ui';
import { UIRouterUpgradeModule } from '@uirouter/angular-hybrid';
import { WidgetsModule } from '@reinform-cdp/widgets';
import { SkeletonModule } from '@reinform-cdp/skeleton';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import * as angular from 'angular';
import { setAngularJSGlobal, UpgradeModule } from '@angular/upgrade/static';
import { AppComponent } from './app.component';
import { ReportComponent } from './forms/report.component';
import { GeneralInformationComponent } from './steps/general-information.component';
import { InformationAboutThePropertyComponent } from './modals/information-about-the-property.component';
import { BuildingPermitsComponent } from './modals/building-permits.component';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { statesConfig } from './states.config';
import { GeneralInformationErrorComponent } from './modals/general-information-error.component';
import { InsuranceContractsComponent } from './modals/insurance-contracts.component';
import { CreditOrganizationComponent } from './modals/credit-organization.component';
import { LoanCompanyComponent } from './modals/loan-company.component';
import { ScheduleWorksComponent } from './components/schedule-works.component';
import { ScheduleWorkComponent } from './modals/schedule-work-component';
import { BalanceComponent } from './steps/balance.component';
import { BalanceTableComponent } from './components/balance-table.component';
import { ErrorComponent } from './modals/error.component';
import { RatesComponent } from './steps/rates.component';
import { RatesBalanceTableComponent } from './components/rates-balance-table.component';
import { RatesH1Component } from './components/rates-h1.component';
import { RatesH2Component } from './components/rates-h2.component';
import { RatesNSSComponent } from './components/rates-nss.component';
import { RatesNSSBalanceTableComponent } from './components/rates-nss-balance-table.component';
import { ReferenceComponent } from './steps/reference.component';
import { OverdueObligationsComponent } from './modals/overdue-obligations.component';
import { OtherCostsComponent } from './modals/other-costs.component';
import { StatementComponent } from './steps/statement.component';
import { ConsolidatedCumulativeSheetsComponent } from './modals/consolidated-cumulative-sheets.component';
import { ConsolidatedCumulativeTableComponent } from './components/consolidated-cumulative-table.component';
import { ExportComponent } from './steps/export.component';
import { ReportService } from "./models/report.service";

const ngWizardConfig: NgWizardConfig = {
  theme: THEME.default
};

setAngularJSGlobal(angular);

@NgModule({
  declarations: [
    AppComponent,
    ReportComponent,
    GeneralInformationComponent,
    BalanceComponent,
    InformationAboutThePropertyComponent,
    BuildingPermitsComponent,
    GeneralInformationErrorComponent,
    InsuranceContractsComponent,
    CreditOrganizationComponent,
    LoanCompanyComponent,
    ScheduleWorksComponent,
    ScheduleWorkComponent,
    BalanceTableComponent,
    ErrorComponent,
    RatesComponent,
    RatesBalanceTableComponent,
    RatesH1Component,
    RatesH2Component,
    RatesNSSComponent,
    RatesNSSBalanceTableComponent,
    ReferenceComponent,
    OverdueObligationsComponent,
    OtherCostsComponent,
    StatementComponent,
    ConsolidatedCumulativeSheetsComponent,
    ConsolidatedCumulativeTableComponent,
    ExportComponent,
  ],
  imports: [
    FormsModule,
    LaddaModule,
    UpgradeModule,
    ModalModule.forRoot(),
    // NgbModule,
    UIRouterUpgradeModule.forRoot({ states: statesConfig }),
    BlockUIModule.forRoot(),
    BrowserAnimationsModule,
    BsDatepickerModule.forRoot(),
    BrowserModule,
    NgSelectModule,
    FormsModule,
    NgWizardModule.forRoot(ngWizardConfig),
    NgxMaskModule.forRoot(),
    HttpClientModule,
    ButtonsModule.forRoot(),
    WidgetsModule.forRoot(),
    SkeletonModule.forRoot()
  ],
  bootstrap: [AppComponent],
  providers: [
    { provide: APP_BASE_HREF, useValue: '/dol' },
    ReportService
  ],
  entryComponents: [
    InformationAboutThePropertyComponent,
    GeneralInformationErrorComponent,
    BalanceComponent,
    BuildingPermitsComponent,
    InsuranceContractsComponent,
    CreditOrganizationComponent,
    LoanCompanyComponent,
    ScheduleWorksComponent,
    ScheduleWorkComponent,
    BalanceTableComponent,
    ErrorComponent,
    RatesComponent,
    RatesBalanceTableComponent,
    RatesH1Component,
    RatesH2Component,
    RatesNSSComponent,
    RatesNSSBalanceTableComponent,
    ReferenceComponent,
    OverdueObligationsComponent,
    OtherCostsComponent,
    StatementComponent,
    ConsolidatedCumulativeSheetsComponent,
    ConsolidatedCumulativeTableComponent,
    ExportComponent,
  ]
})
export class AppModule {
  // constructor(private upgrade: UpgradeModule) {
  // }

  // ngDoBootstrap() {
  //   this.upgrade.bootstrap(document.body, ['app.module'], { strictDi: false });
  // }
}

// const ng1Module = angular.module('app.module', [
  // 'cdp.widgets',
// ]);

// ng1Module.run([]);
